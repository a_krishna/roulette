import sbtassembly.Plugin.AssemblyKeys._

Project.inConfig(Compile)(baseAssemblySettings)

mainClass in (Compile, assembly) := Some("com.tykhe.app.Application")

jarName in (Compile, assembly) := s"${name.value}-${version.value}-dist.jar"

mergeStrategy in (Compile, assembly) <<= (mergeStrategy in (Compile, assembly)) {
  (old) => {
    case PathList(ps @ _*) if ps.last endsWith ".html" =>MergeStrategy.first
    case "META-INF/MANIFEST.MF" => MergeStrategy.discard
    case "roulette.conf" => MergeStrategy.discard
    case x => old(x)
  }
}
