import Dependencies._
name  :=  "roulette-game"

version  :=  "1.0"

scalaVersion  :=  "2.10.3"



libraryDependencies ++= Seq(
  "commons-collections" % "commons-collections" % "3.2.1",
  "com.typesafe" % "config" % "1.2.0",
  "com.typesafe.akka" % "akka-actor_2.10" % "2.3.11",
  "com.typesafe.akka" % "akka-testkit_2.10" % "2.3.11",
  "com.badlogicgames.gdx" % "gdx" % badlogicgamesVersion,
  "com.badlogicgames.gdx" % "gdx-backend-lwjgl" % badlogicgamesVersion,
  "com.badlogicgames.gdx" % "gdx-platform" % badlogicgamesVersion classifier "natives-desktop",
  "com.badlogicgames.gdx" % "gdx-freetype" % badlogicgamesVersion,
  "com.badlogicgames.gdx" % "gdx-freetype-platform" % badlogicgamesVersion classifier "natives-desktop",
  "org.scalatest" %% "scalatest" % "1.9.1" % "test",
  "junit" % "junit" % "4.12"
)
