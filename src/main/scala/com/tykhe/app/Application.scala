package com.tykhe.app

import akka.actor.{Props, ActorSystem}

/**
 * Created by aravind on 3/7/15.
 */
object Application  extends App{
  val system=ActorSystem("TERMINAL")
  val terminal=system.actorOf(Props[Terminal],"terminal_actor")
  terminal ! Initialize()
}
