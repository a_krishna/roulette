package com.tykhe.app

import java.security.SecureRandom

import akka.actor._
import com.tykhe.roulette.simulation.RouletteActor
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Random

/**
 * Created by aravind on 3/7/15.
 */
trait Event
trait EventRequest extends Event
case class Initialize() extends EventRequest
case class Play() extends EventRequest
case class Stop() extends EventRequest
case class TestPlay() extends EventRequest
case class LoadCredits() extends EventRequest
case class ClearResult() extends EventRequest
case class TurnSchedulerOff() extends EventRequest
case class PlaceBets() extends EventRequest
case class WheelStop() extends EventRequest
case class ShowingWinHighlights() extends EventRequest
case class StopTImer() extends EventRequest

trait EventResponse extends Event
case class Initialized() extends EventResponse
case class CreditInitialized() extends EventResponse
case class ResultUpdated() extends EventResponse
case class Credited() extends EventResponse
case class StopNumber(value:Int) extends EventResponse
case class CreditsRested() extends EventResponse


class Terminal extends Actor with ActorLogging{
  val gameActor=context.actorOf(Props[Game],"game")
   def receive={
     case Initialize()=>
       gameActor ! Initialize()
     case Play()=>gameActor ! Play()
     case Stop()=>
     case Initialized()=>
       log.debug(self+" "+Initialized)
       self ! Play()
    }
}
class Game extends Actor{
  val rouletteUIActor=context.actorOf(Props[RouletteUI],"roulette_ui")
  var actorRef:ActorRef=null
  def receive={
    case Initialize()=>
      actorRef=sender()
      rouletteUIActor ! Initialize()
    case Play()=>
      rouletteUIActor ! Play()
    case Stop()=>
    case Initialized()=>
      actorRef ! Initialized()
  }
}
class RouletteUI extends Actor{
  import context.dispatcher
  val rouletteActor=context.actorOf(Props[RouletteActor],"rouletteActor")
  var actorRef:ActorRef=null
  val rng=new Random()
  def receive={
    case Initialize()=>
      actorRef=sender()
      rouletteActor ! Initialize()
    case Play()=>
      rouletteActor ! LoadCredits()
    case Stop()=>
    case Initialized()=>
      actorRef ! Initialized()
    case CreditInitialized()=>rouletteActor ! LoadCredits()
    case ResultUpdated()=>
    case TurnSchedulerOff()=>
    case Credited()=> rouletteActor ! PlaceBets()
    case CreditsRested()=> self ! Credited()
    case WheelStop()=>
      val num=rng.nextInt(36)
      rouletteActor ! StopNumber(num)
    case ShowingWinHighlights()=> context.system.scheduler.scheduleOnce(5 seconds,rouletteActor,ClearResult())

  }
}
