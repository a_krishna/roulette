package com.tykhe.roulette

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.tykhe.roulette.constant.Constants
import com.typesafe.config.Config


case class FontBorder(color:Color,width:Float,straight:Boolean,glyphCount:Int)
case class FontShadow(color: Color,offsetX:Int,offsetY:Int,glyphCount:Int)
object GenerateFont {
  def fontStyle(fontName:String,size:Int,color: Color):LabelStyle={
    new LabelStyle(fontGenerator(fontName).generateFont(fontParameter(new FreeTypeFontParameter(),size)),color)
  }
  def fontGenerator(fontName:String):FreeTypeFontGenerator=new FreeTypeFontGenerator(Gdx.files.internal(fontName))
  def fontParameter(fontParameter:FreeTypeFontParameter,size:Int):FreeTypeFontParameter={
    fontParameter.borderColor=Color.BLUE
    fontParameter.kerning=true
    fontParameter.borderWidth=4f
//    fontParameter.borderStraight=
//    fontParameter.borderGlyphCount=4
    fontParameter.size=size
    fontParameter
  }

  def loadDecoratedFontStyle (config: Config):LabelStyle ={
    new LabelStyle(fontGenerator(config.getString("name")).generateFont(generateFontParameter(loadFontParameter(config.getConfig("border"),config.getConfig("shadow")),config.getInt("size"))),Constants.colorConstant(config.getString("color")))
  }
  def loadFontParameter(border: Config,shadow: Config) =  (FontBorder(Constants.colorConstant(border.getString("color")),border.getLong("width"),border.getBoolean("straight"),border.getInt("glyphCount")),FontShadow(Constants.colorConstant(shadow.getString("color")),shadow.getInt("offsetX"),shadow.getInt("offsetY"),shadow.getInt("glyphCount")))
  def generateFontParameter(fp:(FontBorder,FontShadow),size:Int)={
    val fontParameter=new FreeTypeFontParameter
    fontParameter.borderColor=fp._1.color
    fontParameter.borderGlyphCount=fp._1.glyphCount
    fontParameter.borderStraight=fp._1.straight
    fontParameter.borderWidth=fp._1.width
    fontParameter.shadowColor=fp._2.color
    fontParameter.shadowGlyphCount=fp._2.glyphCount
    fontParameter.shadowOffsetX=fp._2.offsetX
    fontParameter.shadowOffsetY=fp._2.offsetY
    fontParameter.size=size
    fontParameter
  }

}
