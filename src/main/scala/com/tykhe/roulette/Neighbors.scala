package com.tykhe.roulette

/**
 * Created by aravind on 3/6/15.
 */
object TrackNeighbors {
  var siblings=1
val track= Array(0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26)
  def takeNeighbors(value: Int): Array[Int] = {
    def leftT(left:Int)(leftCount:Int):Array[Int]={
      val k=track.indexOf(left)
      if(leftCount==0) Array(left) else  leftT(if(k-1 < 0 ) track.takeRight(1).head else track(k-1))(leftCount-1) ++ Array(left)
    }
    def rightT(right:Int)(rightCount:Int):Array[Int]={
      val k=track.indexOf(right)
      if(rightCount==0) Array(right) else Array(right) ++ rightT(if(k+1 >= track.length ) track.head else track(k+1))(rightCount-1)
    }
    leftT(value)(siblings)++rightT(value)(siblings).drop(1)
  }
}
