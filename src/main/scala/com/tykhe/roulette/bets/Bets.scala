package com.tykhe.roulette.bets

import com.tykhe.roulette.components.BetConstant

trait Bet
trait OutsideBet extends Bet
case class Even() extends OutsideBet
case class Odd() extends OutsideBet
case class ColumnOne() extends OutsideBet
case class ColumnTwo() extends OutsideBet
case class ColumnThree() extends OutsideBet
case class RowOne() extends OutsideBet
case class RowTwo() extends OutsideBet
case class RowThree() extends OutsideBet
case class Manque() extends OutsideBet
case class Passe() extends OutsideBet
case class Red() extends OutsideBet
case class Black() extends OutsideBet

trait InsideBet extends Bet
case class StraightUp(value:Int) extends InsideBet
case class  Split(value: (Int, Int)) extends InsideBet
case class Corner(value:((((Int,Int),Int),Int))) extends InsideBet
case class Street(value:((Int,Int),Int)) extends InsideBet
case class Basket(value:((Int,Int),Int)) extends InsideBet
case class TopLine(value:(Int,Int,Int,Int)) extends InsideBet
case class DoubleStreet(value:(Street,Street)) extends InsideBet

trait CalledBet extends Bet
case class CallBet(value:(Bet,Int))
case class ZeroGame(callBet: Seq[CallBet]) extends CalledBet
case class NeighboursOfZero(callBet: Seq[CallBet]) extends CalledBet
case class Orphans(callBet: Seq[CallBet]) extends CalledBet
case class ThirdWheel(callBet: Seq[CallBet]) extends CalledBet
case class MaxBet(callBet: Seq[CallBet]) extends  CalledBet

trait NeighborsBet extends Bet
case class Neighbors(value:Int) extends NeighborsBet

object InsideBet {
    val straightUps = (0 to 36).map(StraightUp)
    val splits = Seq(0).zip(BetConstant.ROW_ONE().take(1)).map(Split) ++ Seq(0).zip(BetConstant.ROW_TWO().take(1)).map(Split) ++ Seq(0).zip(BetConstant.ROW_THREE().take(1)).map(Split) ++ BetConstant.ROW_ONE().zip(BetConstant.ROW_ONE().drop(1)).map(Split) ++ BetConstant.ROW_TWO().zip(BetConstant.ROW_TWO().drop(1)).map(Split) ++ BetConstant.ROW_THREE().zip(BetConstant.ROW_THREE().drop(1)).map(Split) ++ BetConstant.ROW_ONE().zip(BetConstant.ROW_TWO()).map(Split) ++ BetConstant.ROW_TWO().zip(BetConstant.ROW_THREE()).map(Split)
    val baskets =Seq(0,0).zip(BetConstant.NUM()).zip(BetConstant.NUM().drop(1)).map(Basket)
    val streets= BetConstant.ROW_ONE().zip(BetConstant.ROW_TWO()).zip(BetConstant.ROW_THREE()).map(Street)
    val corners = BetConstant.ROW_ONE().zip(BetConstant.ROW_TWO()).zip(BetConstant.ROW_ONE().drop(1)).zip(BetConstant.ROW_TWO().drop(1)).map(Corner) ++ BetConstant.ROW_TWO().zip(BetConstant.ROW_THREE()).zip(BetConstant.ROW_TWO().drop(1)).zip(BetConstant.ROW_THREE().drop(1)).map(Corner)
    val topLine = TopLine(0,1,2,3)
    val doubleStreet = streets.zip(streets.drop(1)).map(DoubleStreet)
}
object CalledBet{
  val zeroGame=Seq(Split(0,3)).zip(Seq(1)).map(CallBet) ++ Seq(Split(12,15)).zip(Seq(1)).map(CallBet) ++ Seq(Split(32,35)).zip(Seq(1)).map(CallBet) ++ Seq(StraightUp(26)).zip(Seq(1)).map(CallBet)
  val neighborsOfZero= Seq(Split(4,7)).zip(Seq(1)).map(CallBet) ++ Seq(Split(12,15)).zip(Seq(1)).map(CallBet) ++ Seq(Split(18,21)).zip(Seq(1)).map(CallBet) ++ Seq(Split(19,22)).zip(Seq(1)).map(CallBet) ++ Seq(Split(32,35)).zip(Seq(1)).map(CallBet) ++ Seq( Basket(((0,2),3))).zip(Seq(2)).map(CallBet) ++ Seq(Corner((((25,26),28),29))).zip(Seq(2)).map(CallBet)
  val orphans=Seq(Split(6,9)).zip(Seq(1)).map(CallBet) ++ Seq(Split(14,17)).zip(Seq(1)).map(CallBet) ++ Seq(Split(17,20)).zip(Seq(1)).map(CallBet) ++ Seq(Split(31,34)).zip(Seq(1)).map(CallBet) ++ Seq(StraightUp(1)).zip(Seq(1)).map(CallBet)
  val thirdWheel= Seq(Split(5,8)).zip(Seq(1)).map(CallBet) ++ Seq(Split(10,11)).zip(Seq(1)).map(CallBet) ++ Seq(Split(13,16)).zip(Seq(1)).map(CallBet) ++ Seq(Split(23,24)).zip(Seq(1)).map(CallBet) ++ Seq(Split(27,30)).zip(Seq(1)).map(CallBet) ++ Seq(Split(33,36)).zip(Seq(1)).map(CallBet)
  val maxBet= Seq(Split(17,14)).zip(Seq(2)).map(CallBet) ++ Seq(Split(17,17)).zip(Seq(2)).map(CallBet) ++ Seq(Split(17,18)).zip(Seq(2)).map(CallBet) ++ Seq(Split(17,20)).zip(Seq(2)).map(CallBet) ++ Seq(Street((17,16),18)).zip(Seq(3)).map(CallBet) ++ Seq(Corner((((17,16),13),14))).zip(Seq(4)).map(CallBet) ++ Seq(Corner((((17,18),14),15))).zip(Seq(4)).map(CallBet) ++ Seq(Corner((((17,16),19),20))).zip(Seq(4)).map(CallBet) ++ Seq(Corner((((17,18),20),21))).zip(Seq(4)).map(CallBet) ++ Seq(DoubleStreet(Street((17,16),18),Street((13,14),15))).zip(Seq(6)).map(CallBet) ++ Seq(DoubleStreet(Street((17,16),18),Street((19,20),21))).zip(Seq(6)).map(CallBet) ++ Seq(StraightUp(17)).zip(Seq(1)).map(CallBet)
  def processBet(bet: CalledBet):Seq[CallBet]=bet match {
      case ZeroGame(a)=>a
      case NeighboursOfZero(a)=>a
      case Orphans(a)=>a
      case ThirdWheel(a)=>a
      case MaxBet(a)=>a
    }
}

object WinPrediction{
  def predictWin(value:Int)= InsideBet.straightUps.filter(_ == StraightUp(value)) ++ InsideBet.splits.filter(e=>compareTo(e.value,value)) ++ InsideBet.baskets.filter(e=>compareTriple(e.value,value)) ++ InsideBet.streets.filter(e=>compareTriple(e.value,value)) ++ InsideBet.corners.filter(e=>compareCorner(e.value,value)) ++ InsideBet.doubleStreet.filter(e=>compareStreet(e.value,value)) ++ compareTop(InsideBet.topLine.value,value) ++ BetConstant.prepareOutsideWin(value)

  def compareTo(value:(Int,Int),rev:Int):Boolean={
    value._1 == rev match {
      case true=>true
      case false => value._2 == rev match {
        case true=>true
        case false=>false
      }
    }
  }
  def compareCorner(value:((((Int,Int),Int),Int)),rev:Int):Boolean={
    compareTriple(value._1,rev) match {
      case true=>true
      case false=> value._2 == rev match {
        case true=>true
        case false=>false
      }
    }

  }
  def compareTriple(value:((Int,Int),Int),rev:Int):Boolean={
    compareTo(value._1,rev) match {
      case true=>true
      case false=>value._2 == rev match {
        case true=>true
        case false=>false
      }
    }
  }
  def compareStreet(value:(Street,Street),rev:Int):Boolean={
    compareTriple(value._1.value,rev) match {
      case true=>true
      case false=>compareTriple(value._2.value,rev)
    }
  }
  def compareTop(value:(Int,Int,Int,Int),rev:Int):Seq[Bet]={
      value._1 == rev match {
        case true=> Seq(TopLine(value))
        case false=>value._2 == rev match {
          case true=> Seq(TopLine(value))
          case false => value._3 == rev match {
            case true=> Seq(TopLine(value))
            case false=> value._4 == rev match {
              case false=>Seq()
              case true=> Seq(TopLine(value))
            }
          }
        }
      }
  }
}
object PayOut{
  val straightUp = 35
  val split = 17
  val basket = 11
  val street = 11
  val corner = 8
  val topLine = 6
  val sixLine = 5
  val matrix = 2
  val lowWin = 1
  def payOutCalculation(bet: Bet,amount:Int):Float={
  bet match {
    case StraightUp(_)=> (amount * straightUp).toFloat
    case Split(_)=> (amount * split).toFloat
    case Corner(_)=>(amount * corner).toFloat
    case Street(_)=>(amount * street).toFloat
    case DoubleStreet(_)=>(amount * sixLine).toFloat
    case Basket(_)=>(amount * basket).toFloat
    case TopLine(_)=>(amount * topLine).toFloat
    case ColumnOne()=>(amount * matrix).toFloat
    case ColumnTwo()=>(amount * matrix).toFloat
    case ColumnThree()=>(amount * matrix).toFloat
    case RowOne()=>(amount * matrix).toFloat
    case RowTwo()=>(amount * matrix).toFloat
    case RowThree()=>(amount * matrix).toFloat
    case Manque()=>(amount * lowWin).toFloat
    case Passe()=>(amount * lowWin).toFloat
    case Even()=>(amount * lowWin).toFloat
    case Odd()=>(amount * lowWin).toFloat
    case Red()=>(amount * lowWin).toFloat
    case Black()=>(amount * lowWin).toFloat
    case _=>amount * 0
  }
  }
}
