package com.tykhe.roulette.bets

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion
import com.badlogic.gdx.graphics.g2d.{SpriteBatch, Batch, TextureRegion}
import com.badlogic.gdx.scenes.scene2d.{Group, Actor}
import com.tykhe.roulette.TrackNeighbors
import com.tykhe.roulette.components.BetConstant

/**
 * Created by aravind on 28/5/15.
 */
object HighLights {
  def prepareHighlights(bet: Bet):Array[Bet]={
    bet match {
      case StraightUp(a)=>Array(StraightUp(a))
      case Split(a)=>Array(StraightUp(a._1),StraightUp(a._2))
      case Street(a)=>Array(StraightUp(a._1._1),StraightUp(a._1._2),StraightUp(a._2))
      case Corner(a)=>Array(StraightUp(a._1._1._1),StraightUp(a._1._1._2),StraightUp(a._1._2),StraightUp(a._2))
      case DoubleStreet(a)=>prepareHighlights(a._1) ++ prepareHighlights(a._2)
      case Basket(a)=>Array(StraightUp(a._1._1),StraightUp(a._1._2),StraightUp(a._2))
      case TopLine(a)=>Array(StraightUp(a._1),StraightUp(a._2),StraightUp(a._3),StraightUp(a._4))
      case Even()=>(0 to 36).filter(_%2==0).foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case Odd()=>(0 to 36).filter(_%2!=0).foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case Manque()=>BetConstant.MANQUE().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case Passe()=>BetConstant.PASSE().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case RowOne()=>BetConstant.ROW_ONE().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case RowTwo()=>BetConstant.ROW_TWO().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case RowThree()=>BetConstant.ROW_THREE().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case ColumnOne()=>BetConstant.COLUMN_ONE().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case ColumnTwo()=>BetConstant.COLUMN_TWO().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case ColumnThree()=>BetConstant.COLUMN_THREE().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case Red()=>BetConstant.RED().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case Black()=>BetConstant.BLACK().foldLeft(Array.empty[Bet])((a,z)=>a ++ Array(StraightUp(z)))
      case c:CalledBet=>c match {
        case ZeroGame(a)=>a.foldLeft(Array.empty[Bet])((z,a)=>z++prepareHighlights(a.value._1))
        case Orphans(a)=>a.foldLeft(Array.empty[Bet])((z,a)=>z++prepareHighlights(a.value._1))
        case NeighboursOfZero(a)=>a.foldLeft(Array.empty[Bet])((z,a)=>z++prepareHighlights(a.value._1))
        case ThirdWheel(a)=>a.foldLeft(Array.empty[Bet])((z,a)=>z++prepareHighlights(a.value._1))
      }
      case n:NeighborsBet=> n match {
        case Neighbors(a)=>TrackNeighbors.takeNeighbors(a).map(Neighbors).foldLeft(Array.empty[Bet])((z,a)=>z++Array(a)++Array(StraightUp(a.value)))
      }
    }
  }
}
case class TextureProperties(region:TextureRegion,posX:Int,posY:Int)
case class BetHighLight(properties: TextureProperties) extends Actor{
  var flag=false
  override def draw(batch:Batch,parentAlpha:Float)={
    super.draw(batch,parentAlpha)
    flag match {
      case true=>
        properties.region.asInstanceOf[AtlasRegion].rotate match {
          case true=>batch.draw(properties.region, properties.posX+147, properties.posY-3,0 ,0 , properties.region.getRegionWidth(), properties.region.getRegionHeight(), 1, 1, 90, true);
          case false=>  batch.draw(properties.region,properties.posX,properties.posY)
        }
      case false=>
    }
  }
  override  def toString:String=properties.toString
}
case class HighLights(highLights:Array[(Bet,BetHighLight)]) extends Group{
  highLights.foreach(e=>addActor(e._2))
  def showHighLights(bets:Array[Bet]):Array[(Int,Int)]={reset;bets.foldLeft(Array.empty[(Int,Int)]){(z,e)=>z++highLights.filter(_._1==e).foldLeft(Array.empty[(Int,Int)]){(r,t)=>t._2.flag=true;r ++ Array((t._2.properties.posX,t._2.properties.posY))}}}
  def reset:Unit=highLights.foreach(e=>e._2.flag=false)
}

//batch.draw(properties.region, properties.posX+147, properties.posY-3,0 ,0 , properties.region.getRegionWidth(), properties.region.getRegionHeight(), 1, 1, 90, true);