package com.tykhe.roulette.components

import com.badlogic.gdx.audio.Music.OnCompletionListener
import com.badlogic.gdx.audio.{Music, Sound}
import com.badlogic.gdx.scenes.scene2d.{Event, Actor}

/**
 * Created by aravind on 18/5/15.
 */
trait PlayCompleteOff extends OnCompletionListener
case class SoundCompleteListener() extends Event
object AudioSetting{
  var volume,pitch:Float=1.0F
  val pan=0F
  var flag=false
}
case class Audio(sound:Sound) extends Actor with PlayCompleteOff{
def playSound={
  val soundId: Long = sound.play
  sound.setVolume(soundId, AudioSetting.volume);
  sound.setPitch(soundId, AudioSetting.pitch);
  sound.setPan(soundId, AudioSetting.pan, AudioSetting.volume);
  if (AudioSetting.flag) sound.loop()
}
  override  def onCompletion(music: Music)={
    fire(SoundCompleteListener())
  }
}
