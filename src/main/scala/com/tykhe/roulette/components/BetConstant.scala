package com.tykhe.roulette.components

import com.badlogic.gdx.graphics.Color
import com.tykhe.roulette.bets.{ColumnOne,ColumnTwo,ColumnThree,RowOne,RowTwo,RowThree,Red,Black,Even,Odd,Manque,Passe, Bet}

object BetConstant {
  def RED() = Array(1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36)
  def BLACK() = Array(2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35)
  def COLUMN_ONE() = 1 to 12
  def COLUMN_TWO() = 13 to 24
  def COLUMN_THREE() = 25 to 36
  def ROW_ONE() = prepareArray(1,3,new Array[Int](12))
  def ROW_TWO() = prepareArray(2,3,new Array[Int](12))
  def ROW_THREE() = prepareArray(3,3,new Array[Int](12))
  def MANQUE() = 1 to 18
  def PASSE() = 19 to 36
  def NUM():Array[Int]=Array(1,2,3)
  def columnBets(value:Int):Seq[Bet]={
    value > 0 match {
      case false => Seq()
      case true => COLUMN_ONE().contains(value) match {
        case true => Seq(ColumnOne())
        case false => COLUMN_TWO().contains(value) match {
          case true => Seq(ColumnTwo())
          case false => Seq(ColumnThree())
        }
      }
    }
  }
  def rowBets(value:Int):Seq[Bet]={
    value > 0 match {
      case false => Seq()
      case true => ROW_ONE().contains(value) match {
        case true => Seq(RowOne())
        case false => ROW_TWO().contains(value) match {
          case true => Seq(RowTwo())
          case false => Seq(RowThree())
        }
      }
    }
  }
  def colorsBets(value: Int):Seq[Bet]= value > 0 match {case false=>Seq() case true=>RED().contains(value) match {case true=>Seq(Red()) case false=>Seq(Black())}}
  def evenOdd(value: Int):Seq[Bet]=value > 0 match { case false=>Seq() case true=>value % 2 match { case 0=>Seq(Even()) case _=>Seq(Odd())}}
  def series(value:Int):Seq[Bet]=value > 0 match { case false=>Seq() case true=> MANQUE().contains(value) match{ case true=>Seq(Manque()) case false=>Seq(Passe())}}
  def prepareOutsideWin(value:Int):Seq[Bet]=columnBets(value) ++ rowBets(value) ++ colorsBets(value) ++ evenOdd(value) ++ series(value)
  def getColor(num:Int):Color=colorsBets(num) match {
    case Seq(Red())=> Color.RED
    case Seq(Black())=>Color.BLACK
    case _=>Color.GREEN
  }

  def prepareArray(start:Int,pad:Int,array:Array[Int])= {
    (0 to 11).foldLeft(start){(z,a)=>array(a)=z;z+pad}
    array
  }
}
