package com.tykhe.roulette.components

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group}
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.tykhe.roulette.components.button.state._
import com.tykhe.roulette.components.buttons.Denom
import com.tykhe.roulette.core.{STOP, Roulette}
import com.typesafe.config.Config
import scala.collection.JavaConversions._

case class ButtonPressed(name:String) extends Event
object ButtonState{
  var multipleButtonState=""
  def setDenomination(denomProp:Map[String,Int],state:String)={
    state.split("-").contains("chip") match {
      case true=>Denom.currentDenomination=denomProp(state.split("-").take(1).head);multipleButtonState=state
      case false=>Denom.currentDenomination=denomProp(state.split("-").take(1).head);multipleButtonState=state.split("-").dropRight(1).foldLeft("")((a,z)=>a+"-"+z).drop(1)
    }
    println(Denom.currentDenomination)
  }
}
object ButtonConfig{
  import scala.collection.JavaConversions._
  def loadAudio(config: Config):Audio=new Audio(Gdx.audio.newSound(Gdx.files.internal(config.getString("path"))))
  def loadButtons(config: Config,skin: Skin):(Skin,Array[(String,ButtonStyle)])=(skin,config.getStringList("styles").toList.foldRight(Array.empty[(String,ButtonStyle)])((z,a)=> Array((z,skin.get(z,classOf[ImageButtonStyle])))++a))
  def produceAudioButton(config: Config,skin: Skin):(Audio,StateFullButton)={
    val stateFullButton=new StateFullButton(loadButtons(config,skin))
    stateFullButton.setPosition(config.getLong("x"),config.getLong("y"))
    return (loadAudio(config),stateFullButton)
  }
  def setVolume(state:String)=state match {
    case "high"=>  AudioSetting.volume=1.0f
    case "medium"=> AudioSetting.volume=0.5F
    case "low"=> AudioSetting.volume=0.2F
    case "mute"=>AudioSetting.volume=0F
  }
}

class VolumeControlButton(config:Config,skin: Skin) extends Group{
  val button=ButtonConfig.produceAudioButton(config.getConfig("volumeButton"),skin)
  addActor(button._1)
  addActor(button._2)
  button._2.addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      if(event.isInstanceOf[ButtonClicked]){
        val index=button._2.getState.indexOf(button._2.getState.find(_==event.asInstanceOf[ButtonClicked].state).head)
        index + 1 >= button._2.getState.length match {
          case false=>event.getTarget.fire(ChangeState(button._2.getState(index + 1)));ButtonConfig.setVolume(button._2.getState(index + 1).split("-").takeRight(1).head)
          case true=>event.getTarget.fire(ChangeState(button._2.getState(0)));ButtonConfig.setVolume(button._2.getState(0).split("-").takeRight(1).head)
        }
        button._1.playSound
      }
      false
    }
  })
}
class MultipleButton(config: Config,skin: Skin) extends Group{
val multipleButtons=config.getConfigList("multipleButton").toList.foldRight(Array.empty[(Audio,StateFullButton)])((z,a)=>a ++ Array(ButtonConfig.produceAudioButton(z,skin)))
  val chipToAmountPattern=config.getConfigList("multipleButton").foldLeft(Array.empty[(String,Int)])((a,z)=>Array((z.getString("chipName"),z.getInt("chipDenom"))) ++ a).toMap
  multipleButtons.foreach{button=>
    addActor(button._1);addActor(button._2)
    button._2.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        if(event.isInstanceOf[ButtonClicked]){
          Roulette.rouletteState match {
            case STOP=>
            case _=>
              multipleButtons.filter(_._2!=button._2).foreach(e=>e._2.changeState(e._2.getState.tail.head))
              event.getTarget.fire(ChangeState(button._2.getState.head))
              ButtonState.setDenomination(chipToAmountPattern,event.asInstanceOf[ButtonClicked].state)
              button._1.playSound
          }
        }
        false
      }
    })
  }
  multipleButtons.takeRight(1).head._2.fire(ChangeState(multipleButtons.takeRight(1).head._2.getState.head))
  ButtonState.setDenomination(chipToAmountPattern,multipleButtons.takeRight(1).head._2.getState.head)
}
class StateHandleButton(config: Config,skin: Skin) extends Group{
  val button=ButtonConfig.produceAudioButton(config,skin)
  addActor(button._1)
  addActor(button._2)

  button._2.addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      if(event.isInstanceOf[ButtonDown]){
          if(event.asInstanceOf[ButtonDown].state.equals(button._2.getState(0)))
            Roulette.buttonStateController(event.asInstanceOf[ButtonDown].state) match {
              case true=>
                event.getTarget.fire(ChangeState(button._2.getState(1)))
                fire(ButtonPressed(event.asInstanceOf[ButtonDown].state))
                button._1.playSound
              case false=>
            }
      }else if(event.isInstanceOf[ButtonUp]){
        if(event.asInstanceOf[ButtonUp].state.equals(button._2.getState(1)))
          event.getTarget.fire(ChangeState(button._2.getState(0)))
      }
      false
    }
  })
  button._2.fire(ChangeState(button._2.getState(0)))
}
class Buttons(config: Config,skin: Skin) extends Group{
  val buttons=config.getConfigList("buttons").foldLeft(Array.empty[(StateHandleButton,String)])((a,z)=>Array((new StateHandleButton(z,skin),z.getString("name")))++a)
  buttons.foreach(e=>addActor(e._1))
}
