package com.tykhe.roulette.components

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.{Batch, TextureRegion}
import com.badlogic.gdx.scenes.scene2d.{Actor, Event, EventListener, Group}

/**
 * Created by aravind on 6/7/15.
 */
class GloveHighlights(region:TextureRegion) extends Actor{
  setPosition(720.0f,950.0f)
  var actionX,actionY=250.0f
  var showFlag,flag=false
  def tweenAction(x:Float)={
      actionX=x
      showFlag=true
      flag=true
  }
  def reverse()={
    region.flip(true,false)
  }
  def reset()={
    setPosition(720.0f,950.0f)
    actionX=250.0f
    actionY=250.0f
    showFlag=false
    flag=false
  }
  override def draw(batch: Batch,parentAlpha:Float)={
    if(showFlag) {
      batch.draw(region, getX, getY)
      if (getX == actionX && getY == actionY) {
        fire(new ActionCompleted())
      }
    }
  }
  override def act(delta:Float)={
    flag match {
      case true=>
        (actionX <= getX && actionY < getY) match {
        case true=>setPosition(getX-10,getY-10)
        case false=>(actionX >= getX && actionY <  getY) match {
          case true=>setPosition(getX+10,getY-10)
          case false=>
        }
      }
      case false=>
    }
  }
}
case class Dolly(texture:Texture) extends Actor{
  var showFlag=false
  override def draw(batch: Batch,parentAlpha:Float)={
    showFlag match {
      case true=>batch.draw(texture,getX,getY)
      case false=>
    }
  }
  def enable(pos:(Int,Int)): Unit ={
    setPosition(pos._1+20,pos._2+25)
    showFlag=true
  }
  def disable()= showFlag=false

}
case class TweenCompleted() extends Event
case class GloveTween(textures:Array[Texture],dolly:Dolly,timerMeterHandler: TimerMeterHandler) extends Group{
  val gloves=Array(new GloveHighlights(new TextureRegion(textures(0))),new GloveHighlights(new TextureRegion(textures(1))))
  var stateCount=0
  gloves.tail.head.reverse()
  addActor(dolly)
  addActor(timerMeterHandler)
  gloves.foreach{glove=>addActor(glove);glove.addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      event.isInstanceOf[ActionCompleted] match {
        case true=>stateCount+=1; glove.reset(); stateCount > 1 match {case true=>fire(TweenCompleted()) case false=>}
        case false=>
      }
      false
    }
  })}
  def motionTween={
    stateCount=0
      gloves.foldLeft(0){(z,e)=> z%2 match {case 0=> e.tweenAction(20) case _=> e.tweenAction(1420)};z+1 }
  }
}
