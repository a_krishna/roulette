package com.tykhe.roulette.components

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.tykhe.roulette.core.{TextureRegionPosition, Position, Bounds, TexturePosition}
import com.typesafe.config.Config

class ImageLoader(texturePosition: TexturePosition) extends Actor{
  val texture:Texture=new Texture(Gdx.files.internal(texturePosition.texturePath))
  var flag=texturePosition.flag
  def enable() = flag=true
  def disable() = flag=false
  def setBounds= super.setBounds(texturePosition.bounds.position.x,texturePosition.bounds.position.y,texture.getWidth,texture.getHeight)
  override def draw(batch: Batch,parentAlpha:Float) = {
    super.draw(batch,parentAlpha)
    flag match {
      case true=>
        batch.draw(texture,texturePosition.bounds.position.x,texturePosition.bounds.position.y)
      case _=>
    }
  }
}
class ImageRegionLoader(texturePosition: TextureRegionPosition) extends Actor{
  var flag=texturePosition.flag
  def enable() = flag=true
  def disable() = flag=false
  def setBounds= super.setBounds(texturePosition.bounds.position.x,texturePosition.bounds.position.y,texturePosition.textureRegion.getRegionWidth,texturePosition.textureRegion.getRegionHeight)
  override def draw(batch: Batch,parentAlpha:Float) = {
    super.draw(batch,parentAlpha)
    flag match {
      case true=>
        batch.draw(texturePosition.textureRegion,texturePosition.bounds.position.x,texturePosition.bounds.position.y)
      case _=>
    }
  }
}
object ConfigLoader{

  def unloadBounds(config:Config):Bounds = Bounds.apply(unloadPosition(config.getConfig(Position.getClass.getSimpleName.dropRight(1))),config.getLong("padX"),config.getLong("padY"))
  def unloadPosition(config: Config):Position = Position.apply(config.getLong("x"),config.getLong("y"),config.getLong("width"),config.getLong("height"))
}
