package com.tykhe.roulette.components


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.graphics.g2d.{TextureRegion, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d._
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Timer
import com.tykhe.roulette.TrackNeighbors
import com.tykhe.roulette.bets._
import com.tykhe.roulette.components.buttons.Denom
import com.tykhe.roulette.components.meter.Meter
import com.tykhe.roulette.core.{PLACE_BET, Roulette, Position}

import scala.collection.mutable.ListBuffer

abstract class Chip {
  val denominationMap = collection.mutable.Map(Denom.denominationValue.sorted.zip(Seq.fill(Denom.denominationValue.length) {
    0
  }).toMap.toSeq: _*)

  def apply(key: Float): Int = denominationMap(key)

  def change(key: Float, value: Int) = {denominationMap(key) = value; value}
}

case class SmallChips() extends Chip {
  def addChip(key: Float): Unit = {
    change(key,apply(key)+1)
    val sortedDenom= List(denominationMap.toSeq.sortWith(_._1 < _._1):_*).partition(_._1<=key)
    sortedDenom._1.foldLeft((0,sortedDenom._1.head._1)){(z,a)=>if(a._1==z._1){ change(z._2,0); (change(a._1,a._2+1),a._1) }else{ (z._1 + (a._1*a._2).toInt ,a._1)}}
    sortedDenom._2.length > 0 match {
      case true=>
        sortedDenom._2.foldLeft((0,sortedDenom._2.head._1)){(z,a)=>if(a._1==z._1){ change(z._2,0); (change(a._1,a._2+1),a._1) }else{ (z._1 + (a._1*a._2).toInt ,a._1)}}
        sortedDenom._2.filter(_._1==sortedDenom._1.foldLeft(0)((z,b)=>(b._1*b._2).toInt + z)) match {
          case (x)=> x.length > 0 match {
            case true =>sortedDenom._1.foreach(e=>change(e._1,0));x.foreach(z=> addChip(z._1))
            case false=>
          }
        }
      case false=>
    }
  }

  def prepareChips(z:List[(Float,Int)])(sum:Float):Map[Float,Int]={
    val sortedMap= z.filter(_._1 < sum)
    val lastNum=sortedMap.length > 1 match {case true=>sortedMap.takeRight(1).head._1 case false => sortedMap.length match {case 1=>sortedMap.head._1 case _=> 0}  }
    val result=sum % lastNum
    result match {
      case 0=>Map(lastNum.toFloat -> (sum/lastNum).toInt)
      case _=>prepareChips(z.takeRight(1))(sum)
    }
  }
  def sortChips(sum:Float)=prepareChips(List(denominationMap.toSeq.sortWith(_._1< _._1):_*))(sum)
  def clearChip() = denominationMap.foreach(e => change(e._1, 0))
}

case class CallBetPressed(calledBet: CalledBet) extends Event

case class BetPressed(bet: Bet) extends Event

case class ProcessCallBet() extends Event

case class NeighborsBetPressed(value: Int) extends Event

case class ProcessUndo(value: Int, flag: Boolean) extends Event

case class ClearCalledBet(bet: Bet, value: Int, flag: Boolean) extends Event

case class ProcessClearUndo(bet: Bet, value: Int) extends Event

case class HistoryEventClear(bet: Bet, value: Int) extends Event

case class UpdateNeighboursBet() extends Event

object PaddingSmallChips {
  val SMALL_CHIP_MOVE_DELAY = 10

  def paddingValue(bet: Bet): (Int, Int) = {
    bet match {
      case StraightUp(0) => (3, 2)
      case StraightUp(_) => (4, 4)
      case Split(value) => InsideBet.splits.dropRight(24).contains(Split(value)) match {
        case true => InsideBet.splits.take(3).contains(Split(value)) match {
          case true => (5, 4)
          case false => (8, 4)
        }
        case false => (4, 6)
      }
      case Corner(_) => (11, 8)
      case Basket(_) => (4, 4)
      case Street(_) => (4, 6)
      case DoubleStreet(_) => (6, 6)
      case TopLine(_) => (8, 8)
      case ColumnOne() => (2, 6)
      case ColumnTwo() => (2, 6)
      case ColumnThree() => (2, 6)
      case RowOne() => (4, 4)
      case RowTwo() => (4, 4)
      case RowThree() => (4, 4)
      case Manque() => (2, 6)
      case Passe() => (2, 6)
      case Even() => (2, 6)
      case Odd() => (2, 6)
      case Red() => (2, 6)
      case Black() => (2, 6)
    }
  }
}

class ActionCompleted extends Event

case class CustomTexture(imagePath: String) extends Actor {
  val texture: Texture = new Texture(Gdx.files.internal(imagePath))

  override def draw(batch: Batch, parentAlpha: Float) = {
    super.draw(batch, parentAlpha)
    batch.draw(texture, getX, getY)
  }
}


case class CustomTextureRegion(region: TextureRegion) extends Actor {
  var actionX, actionY, value = 0F
  var flag = false

  override def draw(batch: Batch, parentAlpha: Float) = {
    super.draw(batch, parentAlpha)
    batch.draw(region, getX, getY)
    if (getX >= actionX && getY >= actionY) {
      fire(new ActionCompleted)
      flag = false
    }
  }

  override def act(delta: Float) = {
    flag match {
      case true => (actionX > 0 && actionY > 0) match {
        case true => setX(getX + PaddingSmallChips.SMALL_CHIP_MOVE_DELAY); setY(getY + 5)
        case false => actionX > 0 match {
          case true => setX(getX + PaddingSmallChips.SMALL_CHIP_MOVE_DELAY);
          case false => actionX < 0 match {
            case true => setX(getX - PaddingSmallChips.SMALL_CHIP_MOVE_DELAY)
            case false => actionY > 0 match {
              case true => setY(getY + PaddingSmallChips.SMALL_CHIP_MOVE_DELAY)
              case false => actionY < 0 match {
                case true => setY(getY - PaddingSmallChips.SMALL_CHIP_MOVE_DELAY)
                case false =>
              }
            }
          }
        }
      }
      case false =>
    }
  }
}


case class NumberBox(prop: (Bet, Position, Boolean)) extends BoxActionListener(prop._1) {
  setBounds(prop._2.x, prop._2.y, prop._2.width, prop._2.height)
  def createActions(x: Int, y: Int) = {
    var delay = 0.7f
    smallChip.size > 0 match {
      case true => smallChip.toArray.foreach { chip =>
        chip.actionX = x
        chip.actionY = y
        chip.flag = true
        chip.addListener(new EventListener {
          override def handle(event: Event): Boolean = {
            if (event.isInstanceOf[ActionCompleted]) {
              removeActor(chip)
            }
            false
          }
        })
        delay += 0.1f
      }
      case false =>
    }
    reset
  }

  def reset = {
    amount = 0
    padding = 0F
    padTap = (-1F)
    flag = false
    callBetCount=0
    neighborsBetCount=ListBuffer.empty[Int]
  }

  override def draw(batch: Batch, parentAlpha: Float) = {
    super.draw(batch, parentAlpha)
    prop._3 match {
      case true => debug()
      case false =>
    }
  }

  override def toString: String = "{ " + prop + " " + amount + " }"
}

class BoxProperties() extends Group {
  var amount, lastDenom, padding: Float = 0
  var padTap = (-1F)
  var callBetCount=0
  var neighborsBetCount=ListBuffer.empty[Int]
  var flag: Boolean = false
  var smallChip: ListBuffer[CustomTextureRegion] = new ListBuffer[CustomTextureRegion]()
  val refSmallChips = SmallChips()
  val sounds = Roulette.chipsSoundMap.filter(e => e._1 == "singleChip" | e._1 == "multiChips")
  sounds.foreach(z => addActor(z._2))

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}

class BoxActionListener(bet: Bet) extends BoxProperties {
  addListener(new ClickListener() {
    override def clicked(event: InputEvent, x: Float, y: Float) = {
      Roulette.rouletteState match {
        case PLACE_BET =>
          Denom.currentDenomination > 0 match {
            case true =>
              Meter.creditMeter >= Meter.betMeter + Roulette.preBetProcess(bet, Denom.currentDenomination) match {
                case true =>
                  bet match {
                    case c: CalledBet =>
                      Meter.creditMeter >= CalledBet.processBet(c).foldLeft(0)((z, a) => z + a.value._2) * Denom.currentDenomination match {
                        case true =>
                          Meter.betMeter += CalledBet.processBet(c).foldLeft(0)((z, a) => z + a.value._2) * Denom.currentDenomination.toInt
                          fire(CallBetPressed(c))
                          lastDenom = Denom.currentDenomination
                          callBetCount+=1
                        case false =>
                      }
                    case n: NeighborsBet =>
                      Meter.creditMeter >= TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length * Denom.currentDenomination match {
                        case true =>
                          fire(NeighborsBetPressed(n.asInstanceOf[Neighbors].value))
                          Meter.betMeter += TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length * Denom.currentDenomination.toInt
                          lastDenom = Denom.currentDenomination
                          neighborsBetCount+=(TrackNeighbors.siblings)
                        case false =>
                      }
                    case (x) => fire(BetPressed(bet))
                      Meter.betMeter += Denom.currentDenomination.toInt
                      addDenom(Denom.currentDenomination)
                  }
                  Roulette.nombres.+=((bet, lastDenom.toInt))
                  addEvents()
                case false =>
              }

            case false =>
              Meter.betMeter > 0 match {
                case true =>
                  smallChip.length > 0 match {
                    case true =>
                      lastDenom=smallChip.takeRight(1).head.value
                      amount -= lastDenom.toInt
                      removeActor(smallChip.takeRight(1).head)
                      smallChip = smallChip.dropRight(1)
                      println("Length of SmallChip" + smallChip.length + " And Bet" + bet+" last Denom "+lastDenom)
                      bet match {
                        case (x) =>
                          Meter.betMeter -= lastDenom.toInt
                          fire(BetPressed(x))
                      }
                    case false => bet match {
                      case c: CalledBet if callBetCount > 0    =>
                        CalledBet.processBet(c).foldLeft(0)((z, a) => z + a.value._2) * lastDenom.toInt match {
                          case (x) => x < Meter.creditMeter match {
                            case true => Meter.betMeter -= x
                              fire(ClearCalledBet(c, lastDenom.toInt, false))
                              callBetCount-=1
                            case false =>
                          }
                        }
                      case n: NeighborsBet if neighborsBetCount.length > 0 => TrackNeighbors.siblings=neighborsBetCount.takeRight(1).head
                        TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length * lastDenom.toInt match {
                        case (x) => x < Meter.creditMeter match {
                          case true =>
                            fire(UpdateNeighboursBet())
                            Meter.betMeter -= x
                            fire(ClearCalledBet(n, lastDenom.toInt, false))
                            neighborsBetCount=neighborsBetCount.dropRight(1)
                          case false =>
                        }
                      }
                      case _ =>
                    }
                  }
                  Roulette.nombres = Roulette.nombres.-((bet, lastDenom.toInt))
                  Roulette.clearBets.+=((bet, lastDenom.toInt))
                  addEvents()
                case false =>
              }

          }
        case _ =>
      }
    }
  })
  addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      event match {
        case ProcessUndo(v,f) => f match {
          case true => bet match {
            case (x) =>
              addDenom(v)
          }
          case false if smallChip.length > 0=>
            Denom.smallChips.find(_._2 == smallChip.takeRight(1).head).head._1 match {
              case (x)  =>
                println(" Start SMALL CHIP " + smallChip.length + " BET " + bet)
                amount -= x
                removeActor(smallChip.takeRight(1).head)
                smallChip = smallChip.dropRight(1)
                smallChip.length==0 match {case true=>refSmallChips.clearChip() case false=>}
                v < x match {case true =>SmallChips().sortChips(x-v) match {case (z) if z.size > 0=> (0 to z.head._2-1).foreach(e=>addDenom(z.head._1)) }case false=> }
            }
        }
        case ProcessClearUndo(a, b) => a match {
          case c: CalledBet =>
            CalledBet.processBet(c).foldLeft(0)((z, a) => z + a.value._2) * b match {
              case (x) => Meter.creditMeter >= Meter.betMeter + x match {
                case true =>
                  Meter.betMeter += x
                  fire(ClearCalledBet(c, b, true))
                  callBetCount+=1
                case false =>
              }
            }
          case n: NeighborsBet =>
            fire(NeighborsBetPressed(n.asInstanceOf[Neighbors].value))
            TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length * b match {
              case (x) => Meter.creditMeter >= Meter.betMeter + x match {
                case true =>
                  Meter.betMeter += x
                  fire(ClearCalledBet(n, b, true))
                  neighborsBetCount+=(TrackNeighbors.siblings)
                case false =>
              }
            }
          case (x) =>
            Meter.creditMeter >= Meter.betMeter + b match {
              case true =>
                Meter.betMeter += b
                fire(ClearCalledBet(x, b, true))
              case false =>
            }
        }
        case ProcessCallBet() =>
          addDenom(Denom.currentDenomination)
        case HistoryEventClear(e,v)=>clearBettedChip(e,v)
        case _ =>
      }
      false
    }
  })

  def addWinningChips(amount: Float, padding: Float): Float = {
    val texture = new CustomTextureRegion(Denom.smallChips.apply(amount).region)
    texture.setPosition((texture.getX + (getWidth / PaddingSmallChips.paddingValue(bet)._1)) + padding, (texture.getY + (getHeight / PaddingSmallChips.paddingValue(bet)._2)) + padding)
    texture.value = amount
    texture.flag = false
    addChips(texture)
    smallChip += texture
    padding + 0.5f
  }

  def addChips(chip: CustomTextureRegion) = {
    addActor(chip)
  }
  def addEvents()={
    Roulette.undoFlag & Roulette.buttonFlag match {
      case false=>
        Roulette.historyOfButtonEvents+=("BOX")
      case true=>
    }
  }
  def clearBettedChip(bet: Bet,value:Int)={
    bet match {
      case c: CalledBet =>
        CalledBet.processBet(c).foldLeft(0)((z, a) => z + a.value._2) * lastDenom.toInt match {
          case (x) => x < Meter.creditMeter match {
            case true => Meter.betMeter -= x
              fire(ClearCalledBet(c, lastDenom.toInt, false))
            case false =>
          }
        }
      case n: NeighborsBet => TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length * lastDenom.toInt match {
        case (x) => x < Meter.creditMeter match {
          case true => Meter.betMeter -= x
            fire(ClearCalledBet(n, lastDenom.toInt, false))
          case false =>
        }
      }
      case (x) =>
        lastDenom=smallChip.takeRight(1).head.value
        amount -= lastDenom.toInt
        removeActor(smallChip.takeRight(1).head)
        smallChip = smallChip.dropRight(1)
        Meter.betMeter -= lastDenom.toInt
        fire(BetPressed(x))
    }
    Roulette.nombres = Roulette.nombres.-((bet, lastDenom.toInt))
    Roulette.clearBets.+=((bet, lastDenom.toInt))
  }

  def removeChips() = {
    smallChip.foreach { chip => removeActor(chip); smallChip -= chip}
    smallChip.clear()
  }

  def addDenom(denom: Float): Unit = {
    amount += denom
    refSmallChips.addChip(denom)
    smallChip.size > 1 match {
      case true => refSmallChips.denominationMap.foreach(z => z._2 > 0 match {
        case true => smallChip.filter(_.value == z._1).length match {
          case (x) => z._2 <= x match {
            case true => z._2 == x match {
              case true =>
              case false => (x to z._2 - 1).foreach(d => smallChip.filter(_.value == z._1).foreach { rc => smallChip.-=(rc);Roulette.removeMultiDenom+=(this.bet->rc.value.toInt);removeActor(rc)})
            }
            case false => (x to z._2 - 1).foreach(e => addWinningChips(z._1, padding))
          }
        }
        case false => smallChip.filter(_.value == z._1).foreach { rc => smallChip.-=(rc); Roulette.removeMultiDenom+=(this.bet->rc.value.toInt);removeActor(rc)}
      })
        sounds.apply("multiChips").playSound
      case false =>padding=0.0f
        padding += (addWinningChips(denom, padding) - 0.5f)
        sounds.apply("singleChip").playSound
    }
    lastDenom = Denom.smallChips.find(_._2.equals(smallChip.takeRight(1).head)).head._1
  }

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}

class NumberBoxes(boxes: Array[NumberBox]) extends Group {
  val numBoxes = new Array[NumberBox](boxes.length)
  val groupSounds = Roulette.chipsSoundMap.filter(e => e._1 != "singleChip" | e._1 != "multiChips")
  val timer = new Timer
  for (i <- 0 to numBoxes.length - 1) {
    addActor(boxes(i))
    numBoxes(i) = boxes(i)
  }
  groupSounds.foreach(z => addActor(z._2))

  override def draw(batch: Batch, parentAlpha: Float) = super.draw(batch, parentAlpha)
}
