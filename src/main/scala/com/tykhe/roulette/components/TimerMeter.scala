package com.tykhe.roulette.components

import com.badlogic.gdx.scenes.scene2d.{Event, EventListener, Group}
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Skin}
import com.tykhe.roulette.components.meter.Meter
import com.typesafe.config.Config

/**
 * Created by aravind on 7/7/15.
 */
case class Properties(x: Int, y: Int, width: Int, height: Int)

object TimerMeter {
  var value = 6

  def prepareTimerMeter(config: Config, skin: Skin): (Meter, ImageButton, TimerMeter) = {
    value = config.getInt("layout.value")
    (Meter(Meter.prepareMeter(config.getConfig("layout"), skin), Array[Char](0)), prepareImageButton(config, skin), prepareTimer(config, skin))
  }

  def prepareImageButton(config: Config, skin: Skin): ImageButton = {
    val button = new ImageButton(skin.get(config.getString("frontLayout.name"), classOf[ImageButton.ImageButtonStyle]))
    val prop = prepareProperties(config.getConfig("frontLayout.properties"))
    button.setPosition(prop.x, prop.y)
    button.setSize(prop.width, prop.height)
    button
  }

  def prepareTimer(config: Config, skin: Skin): TimerMeter = {
    val timer = TimerMeter(ButtonConfig.loadButtons(config, skin), config.getLong("delay"), config.getConfigList("sizes").toArray[Config](Array.empty[Config]).foldRight(Array.empty[(Int, Int)])((z, e) => Array((z.getInt("x"), z.getInt("y"))) ++ e))
    timer.setPosition(config.getInt("x"), config.getInt("y"))
    timer
  }

  def prepareProperties(config: Config): Properties = Properties(config.getInt("x"), config.getInt("y"), config.getInt("width"), config.getInt("height"))
}

case class ChangeTime(value: Int) extends Event

case class FireWheelRoll() extends Event

case class TimerMeter(config: (Skin, Array[(String, ButtonStyle)]), time: Long, sizes: Array[(Int, Int)]) extends ImageButton(config._1.get(config._2.head._1, classOf[ImageButton.ImageButtonStyle])) {
  val duration = (time * 2) / config._2.length
  var flag = false
  var count = 0
  var sleepTime = 0f
  def triggerTimer() = {
    count = 0
    sleepTime = duration
    flag = true
  }

  def stopTimer() = {
    flag = false
  }

  override def act(delta: Float) = {
    sleepTime += delta
    flag match {
      case true =>
        sleepTime >= duration match {
          case true =>
            sleepTime = 0f
            setStyle(config._2(count)._2)
            setSize(sizes(count)._1, sizes(count)._2)
            TimerMeter.value = TimerMeter.value - 1 < 0 match {
              case true => config._2.length - 1
              case false => TimerMeter.value - 1
            }
            count = count + 1 >= config._2.length match {
              case true => 0
              case false => count + 1
            }
            fire(ChangeTime(TimerMeter.value))
          case false =>
        }
      case false =>
    }
  }
}

case class TimerMeterHandler(config: (Meter, ImageButton, TimerMeter)) extends Group {
  var time = 0.0f
  var seconds = 0
  config._1.setAmount(TimerMeter.value)
  addActor(config._1)
  addActor(config._2)
  addActor(config._3)

  def startTimer() = {
    config._3.addListener(TimeEventListener)
    config._3.triggerTimer()
  }

  object  TimeEventListener extends EventListener{
    override def handle(event: Event)={
      event.isInstanceOf[ChangeTime] match {
        case true => config._1.setAmount(event.asInstanceOf[ChangeTime].value)
          event.asInstanceOf[ChangeTime].value == 0 match {
            case true => fire(FireWheelRoll())
            case false =>
          }
        case false =>
      }
      false
    }
  }
  def removeTimeEventListener()={
    config._3.removeListener(TimeEventListener)
  }

  def stopTimer() = config._3.stopTimer()


}