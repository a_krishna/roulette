package com.tykhe.roulette.components.buttons

import com.badlogic.gdx.graphics.g2d.{TextureRegion, Batch}
import com.badlogic.gdx.scenes.scene2d.{InputEvent, Group}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.tykhe.roulette.components.{ImageRegionLoader, CustomTextureRegion, CustomTexture, ImageLoader}
import com.tykhe.roulette.core.{TextureRegionPosition, Position, Bounds, TexturePosition}

import scala.collection.immutable.HashMap

case class DenomProperties(amounts:Array[Float],smallPath:Array[TextureRegion])

object Denom{
  var denominationValue:Array[Float]=null
  var currentDenomination:Float=0
  var smallChips:Map[Float,CustomTextureRegion]=null
  def apply(denomProperties: DenomProperties)={
    denominationValue=denomProperties.amounts.sortWith(_ < _)
    smallChips=denomProperties.amounts.sorted.zip(denomProperties.smallPath.foldLeft(Array.empty[CustomTextureRegion])((a,z)=>a ++ Array(CustomTextureRegion(z)))).toMap
  }
}
