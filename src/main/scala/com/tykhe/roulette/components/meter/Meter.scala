package com.tykhe.roulette.components.meter


import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{InputEvent, ui, Group}
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Skin, Label}
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.tykhe.roulette.{TrackNeighbors, GenerateFont}
import com.tykhe.roulette.components.BetConstant
import com.tykhe.roulette.constant.Constants
import com.typesafe.config.Config


/**
 * Created by aravind on 26/5/15.
 */
object Meter {
  var winMeter,betMeter,creditMeter=0
  def prepareLabel(x: Int, y: Int, style: LabelStyle, skin: ui.Skin, text: Array[Char]): Label = {
    val label = new Label(text, skin)
    label.setPosition(x, y)
    label.setStyle(style)
    label.setColor(0, 0, 0, 0f)
    label.setText(text)
    label
  }

  def prepareRegion(image: ImageButton, pos: (Int, Int)): ImageButton = {
    image.setPosition(pos._1, pos._2)
    image
  }

  def prepareMeter(config: Config, skin: Skin): (Array[Label], ImageButton) = {
    ((0 to config.getInt("meterSize")).toArray.foldLeft((config.getInt("fontX"), config.getInt("fontY"), config.getInt("padX"), GenerateFont.fontStyle(config.getString("fontName"), config.getInt("fontSize"), Constants.colorConstant(config.getString("fontColor"))), Array.empty[Label]))((a, z) => (a._1 + a._3, a._2, a._3, a._4, Array(prepareLabel(a._1, a._2, a._4, skin, Array[Char](0))) ++ a._5))._5
      , prepareRegion(new ImageButton(skin.get(config.getString("meterName"), classOf[ImageButtonStyle])), (config.getInt("meterX"), config.getInt("meterY"))))
  }
  def prepareLabels(config: Config,skin: Skin):Array[Label]=(0 to config.getInt("meterSize")).toArray.foldLeft((config.getInt("fontX"), config.getInt("fontY"), config.getInt("padX"), GenerateFont.fontStyle(config.getString("fontName"), config.getInt("fontSize"), Constants.colorConstant(config.getString("fontColor"))), Array.empty[Label]))((a, z) => (a._1 + a._3, a._2, a._3, a._4, Array(prepareLabel(a._1, a._2, a._4, skin, Array[Char](0))) ++ a._5))._5
}

case class Meter(meter: (Array[Label], ImageButton), text: Array[Char]) extends Group {
  var value: Int = 0
  addActor(meter._2)
  meter._1.foreach(e => addActor(e))

  val disabledColor = new Color(1, 1, 0.1f, 0.1f)

  def setAmount(amt: Int) = {
    amt < 0 match {
      case true =>
      case false =>
        value = amt
        meter._1.foreach(label => {
          label.setText("0")
          label.setColor(disabledColor)
        })
        amt.toString.toCharArray.reverse.zip(meter._1).foreach(p => {
          p._2.setText(p._1.toString)
          p._2.setColor(Color.WHITE)
        })
//
//        value < amt match {
//          case true => prepareText((value.toInt to amt).toArray)(x => if (x == 0) new Color(1, 1, 0.1f, 0.1f) else Color.WHITE)
//          case false => prepareText((amt to value.toInt).toArray.reverse)(x => if (x == 0) new Color(1, 1, 0.1f, 0.1f) else Color.WHITE)
//        }
//        value = amt
    }
  }

//  def prepareText(amt: Array[Int])(f: Int => Color) = {
//    amt.foreach(e => meter._1.foldLeft(e) { (a, z) =>   z.setText(Constants.intToChar(a % 10).toString); z.setColor(f(a)); a / 10})
//  }

  def reset() = meter._1.foreach { label => label.setColor(1, 1, 0.1f, 0.1f); text(0) = Constants.intToChar(0); label.setText(text)}
}

case class MeterLabel(labels: Array[Label], text: Array[Char]) extends Group {
  var value: Int = 0
  labels.foreach(e => addActor(e))

  def addNumber(num: Int, color: Color) = {
    color match {
      case Color.RED => labels.take(num >=10 match {case true=>2 case false=>1}).foldLeft(num) { (a, z) =>   z.setText(Constants.intToChar(a % 10).toString); z.setColor(color); a / 10}; labels.drop(num >=10 match {case true=>2 case false=>1}).foreach { e =>  e.setText(0.toString); e.setColor(0, 0, 0, 0);}
      case Color.BLACK => labels.takeRight(num >=10 match {case true=>2 case false=>1}).foldLeft(num) { (a, z) =>  z.setText(Constants.intToChar(a % 10).toString); z.setColor(color); a / 10}; labels.dropRight(num >=10 match {case true=>2 case false=>1}).foreach { e =>  e.setText(0.toString); e.setColor(0, 0, 0, 0.1f);}
      case Color.GREEN => labels.drop(2).take(1).foldLeft(num) { (a, z) =>  z.setText(Constants.intToChar(a % 10).toString); z.setColor(color); a / 10}; (labels.take(2) ++ labels.takeRight(2)).foreach { e =>  e.setText(0.toString); e.setColor(0, 0, 0, 0);}
    }
    value = num
  }

  def changePosition(pos: (Float,Float)) = labels.foreach(e => e.setY(pos._2))
}
case class HistoryMeter(meters:Array[MeterLabel],init:Int) extends Group{
  var history=init
  meters.foreach(addActor(_))
  def addDrawnNumber(num:Int): Unit ={
    if(history >= 1) {(0 to history).reverse.foreach(e=>meters(e).addNumber(meters(if(e-1 < 0) 0 else e-1).value,BetConstant.getColor(meters(if(e-1 < 0) 0 else e-1).value)))}
    meters(init).addNumber(num,BetConstant.getColor(num))
    history=if(history+1 >=meters.length) meters.length-1 else history+1
  }
}

class NeighborsSbilingMeters(max:Int,meterProp:((Array[Label], ImageButton),Array[Char])) extends Meter(meterProp._1,meterProp._2){
  setAmount(TrackNeighbors.siblings)
  addListener(new ClickListener(){
    override def clicked(event:InputEvent,x:Float,y:Float):Unit={
        setAmount(if(value.toInt+1 > max) 1 else value.toInt+1)
        TrackNeighbors.siblings=value.toInt
    }
  })
  def update()=setAmount(TrackNeighbors.siblings)
}
case class Meters(config: Config,skin: Skin) extends Group{
  val betMeter=Meter(Meter.prepareMeter(config.getConfig("betMeter"),skin),Array[Char](0))
  val winMeter=Meter(Meter.prepareMeter(config.getConfig("winMeter"),skin),Array[Char](0))
  val creditMeter=Meter(Meter.prepareMeter(config.getConfig("creditMeter"),skin),Array[Char](0))
  val historyMeter=HistoryMeter((0 to 10 ).foldLeft((config.getInt("historyMeter.fontY"),Array.empty[MeterLabel]))((a,z)=>(a._1-config.getInt("historyMeter.padY"),a._2++getMeterLabel(a._1)))._2,0)
  val neighbourMeter=new NeighborsSbilingMeters(config.getInt("neighboursButton.max"),(Meter.prepareMeter(config.getConfig("neighboursButton"),skin),Array[Char](0)))
  addActor(neighbourMeter)
  addActor(historyMeter)
  addActor(betMeter)
  addActor(creditMeter)
  addActor(winMeter)
  def addWinAmount(amt:Int)={
    winMeter.setAmount(amt)
  }
  def addBetAmount(amt:Int)={
    betMeter.setAmount(amt)
  }
  def addCreditAmount(amt:Int)={
    creditMeter.setAmount(amt)
  }
  def getMeterLabel(padY:Int):Array[MeterLabel]={
    val meter=MeterLabel(Meter.prepareLabels(config.getConfig("historyMeter"),skin),Array[Char](0))
    meter.changePosition(meter.getX,padY)
    Array(meter)
  }
  def updateNeighboursMeter()= neighbourMeter.update()
}