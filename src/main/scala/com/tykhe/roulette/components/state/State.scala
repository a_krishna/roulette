package com.tykhe.roulette.components.button.state



import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.{InputEvent, InputListener, Event, EventListener}
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Skin}

/**
 * Created by aravind on 15/5/15.
 */
case class ChangeState(newState:String) extends Event
case class ButtonClicked(state:String) extends Event
case class ButtonUp(state:String) extends Event
case class ButtonDown(state:String) extends  Event
class StateFullButton(style:(Skin,Array[(String,ButtonStyle)])) extends ImageButton( style._1.get( style._2.head._1, classOf[ImageButton.ImageButtonStyle] ) ){
  var state=style._2.head._1
  state.split("-").length match {
    case 2 =>addListener(new InputManager)
    case 3=>addListener(new ClickManager)
    case _=>
  }
  addListener(new ButtonStateManager)
  def getState:Array[String]=style._2.foldLeft("")((z,a)=>a._1+","+z).split(",")
  def changeState(newState:String)={
    state=newState
    setStyle(style._2.filter(_._1==newState).head._2)
  }
  class ClickManager extends ClickListener{
    override def clicked(event:InputEvent,x:Float,y:Float)={
      fire(ButtonClicked(state))
    }
  }

  class InputManager extends InputListener{
    override def touchUp(event:InputEvent,x:Float,y:Float,pointer:Int,button:Int)={
      fire(ButtonUp(state))
    }
    override def touchDown(event:InputEvent,x:Float,y:Float,pointer:Int,button:Int):Boolean={
      fire(ButtonDown(state))
    true
    }
  }

  class ButtonStateManager extends EventListener{
    override def handle(event:Event):Boolean={
      if(event.isInstanceOf[ChangeState]){
        changeState(event.asInstanceOf[ChangeState].newState)
        true
      }
      false
    }
  }
}
