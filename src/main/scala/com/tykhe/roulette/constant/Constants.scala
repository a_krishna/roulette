package com.tykhe.roulette.constant

import com.badlogic.gdx.graphics.Color

/**
 * Created by aravind on 26/5/15.
 */
object Constants {
val colorConstant:String => Color =  _ match {
  case "RED"=>Color.RED
  case "GREEN"=>Color.GREEN
  case "BLUE"=>Color.BLUE
  case "BLACK"=>Color.BLACK
  case "ORANGE"=>Color.ORANGE
  case "YELLOW"=>Color.YELLOW
  case "PINK"=>Color.PINK
  case "MAROON"=>Color.MAROON
  case "MAGENTA"=>Color.MAGENTA
  case "CYAN"=>Color.CYAN
  case "GRAY"=>Color.GRAY
  case "PURPLE"=>Color.PURPLE
  case "L-GRAY"=>Color.LIGHT_GRAY
  case "D-GRAY"=>Color.DARK_GRAY
  case "OLIVE"=>Color.OLIVE
  case "TEAL"=>Color.TEAL
  case "NAVY"=>Color.NAVY
  case "CLEAR"=>Color.CLEAR
  case _=>Color.WHITE
}
  def intToChar(num:Int):Char=num match {
    case 1=>'1'
    case 2=>'2'
    case 3=>'3'
    case 4=>'4'
    case 5=>'5'
    case 6=>'6'
    case 7=>'7'
    case 8=>'8'
    case 9=>'9'
    case _=>'0'
  }
}
