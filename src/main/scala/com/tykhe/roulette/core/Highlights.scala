package com.tykhe.roulette.core

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.g2d.{TextureRegion, Batch}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Group
import com.tykhe.roulette.components.{BetConstant, ImageLoader, ConfigLoader}
import com.typesafe.config.{ConfigList, Config}


case class TexturePosition(bounds: Bounds,texturePath:String,flag:Boolean)
case class TextureRegionPosition(bounds: Bounds,textureRegion:TextureRegion,flag:Boolean)
object Region{
  case class Region(name:String,place:Int,texturePosition: TexturePosition) extends ImageLoader(texturePosition)
  def prepare(config: Config,size:Int): Array[Region] ={
    val objectList=config.getObjectList(Region.getClass.getSimpleName.dropRight(1))
    var regions=new Array[Region](size)
    var j=0;
    for( i <- 0 to objectList.size()-1 ){
      val config= objectList.get(i).toConfig
      var padY:Long=0
      for( row <- 0 to config.getInt("row")-1 ){
        val texturePosition=TexturePosition.apply(ConfigLoader.unloadBounds(config.getConfig(Bounds.getClass.getSimpleName.dropRight(1))),config.getString("texturePath"),config.getBoolean("flag"))
        var padX:Long=0
        var count=row+config.getInt("init")
          for( column <-  0 to config.getInt("column")-1 ){
            regions(j)=new Region(config.getString("name"),count,TexturePosition.apply(Bounds.apply(Position.apply(texturePosition.bounds.position.x+padX,texturePosition.bounds.position.y+padY,texturePosition.bounds.position.width,texturePosition.bounds.position.height),texturePosition.bounds.padX,texturePosition.bounds.padY),texturePosition.texturePath,texturePosition.flag))
            padX+=texturePosition.bounds.padX
            count+=config.getInt("add")
            j+=1
          }
        padY+=texturePosition.bounds.padY
      }
    }
    regions
  }
}
