package com.tykhe.roulette.core

import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.tykhe.roulette.TrackNeighbors
import com.tykhe.roulette.bets._
import com.tykhe.roulette.components.{Audio, NumberBoxes, BetConstant, NumberBox}
import com.typesafe.config.Config

import scala.collection.mutable.ListBuffer

/**
 * Created by aravind on 5/6/15.
 */
trait State

object PLAY extends State

object PLACE_BET extends State

object STOP_BET extends State

object STOP extends State

object Roulette {
  var rouletteState: State = STOP
  var undoFlag = false
  var buttonFlag = false
  var lastBetPlace: Seq[(Bet, Int)] = Seq()
  var lastBetAmount = 0
  var historyOfButtonEvents: ListBuffer[String] = new ListBuffer[String]()
  var nombres: ListBuffer[(Bet, Int)] = new ListBuffer[(Bet, Int)]()
  var removeMultiDenom: ListBuffer[(Bet, Int)] = new ListBuffer[(Bet, Int)]()
  var clearBets: ListBuffer[(Bet, Int)] = new ListBuffer[(Bet, Int)]()
  var chipsSoundMap: Map[String, Audio] = null

  def buttonStateController(state: String): Boolean = rouletteState match {
    case PLACE_BET => state.split("-").head match {
      case "undo" => clearBets.length > 0 match {
        case true => true
        case false => nombres.length > 0
      }
      case "double" => clearBets.length > 0 match {
        case true if nombres.length > 0 => true
        case false => false
      }
      case "rebet" => lastBetPlace.length > 0
      case "clearall" => nombres.length > 0
      case "max" => false
      case "menu" => true
      case _ => false
    }
    case _ => state.split("-").head match {
      case "menu" => true
      case _ => false
    }
  }

  def reset() = {
    nombres.clear()
    clearBets.clear()
    historyOfButtonEvents.clear()
  }

  def preBetProcess(bet: Bet, denom: Float): Float = {
    bet match {
      case cB: CalledBet => cB match {
        case ZeroGame(a) => a.foldLeft(0.0f)((z, a) => z + a.value._2 * denom)
        case NeighboursOfZero(a) => a.foldLeft(0.0f)((z, a) => z + a.value._2 * denom)
        case Orphans(a) => a.foldLeft(0.0f)((z, a) => z + a.value._2 * denom)
        case ThirdWheel(a) => a.foldLeft(0.0f)((z, a) => z + a.value._2 * denom)
        case MaxBet(a) => a.foldLeft(0.0f)((z, a) => z + a.value._2 * denom)
      }
      case Neighbors(a) => TrackNeighbors.siblings * denom
      case _ => denom
    }
  }
  def removeRowThree(value:Int):Boolean= BetConstant.ROW_THREE().contains(value)


  def prepareRoulette(roulette: Config, skin: Skin): (NumberBoxes, HighLights) = {
    val paddingX = 110
    val highLightsPadding = (109, 110, 143, 141)
    val paddingY = (145, 141)
    val cornerPadX = 110
    val cornerPadY = 143
    val streetPadX = 110
    val streetPadY = 408
    (new NumberBoxes(Array(NumberBox(InsideBet.straightUps.take(1).head, Position(49, 330, 70, 419), roulette.getBoolean("roulette.debug"))) ++ InsideBet.straightUps.drop(1).foldLeft((160, 354, Array.empty[NumberBox]))((a, z) => ((if ((z.value % 2) != 0) {
      if (a._2 + paddingY._1 >= paddingY._1 * 5) a._1 + paddingX else a._1
    } else {
      if (a._2 + paddingY._2 >= paddingY._2 * 5) a._1 + paddingX else a._1
    }),
      (if ((z.value % 2) != 0) {
      if (a._2 + paddingY._1 >= paddingY._1 * 5) 354 else a._2 + paddingY._1
    } else {
      if (a._2 + paddingY._2 >= paddingY._2 * 5) 354 else a._2 + paddingY._2
    }),
      Array(NumberBox((z, Position(a._1, a._2, 70, 90), roulette.getBoolean("roulette.debug")))) ++ a._3))._3 ++ InsideBet.splits.take(3).foldLeft((120, 354, 0, Array.empty[NumberBox]))((a, z) => (a._1, (if ((a._3 % 2) == 0) {
      if (a._2 + paddingY._1 >= paddingY._1 * 5) 354 else a._2 + paddingY._1
    } else {
      if (a._2 + paddingY._2 >= paddingY._2 * 5) 354 else a._2 + paddingY._2
    }), a._3 + 1, Array(NumberBox(z, Position(a._1, a._2, 37, 90), roulette.getBoolean("roulette.debug"))) ++ a._4))._4 ++ InsideBet.baskets.foldLeft((120, 450, Array.empty[NumberBox]))((a, z) => (a._1, a._2 + cornerPadY, Array(NumberBox(z, Position(a._1, a._2, 37, 45), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ InsideBet.splits.drop(3).dropRight(24).foldLeft((234, 354, 0, Array.empty[NumberBox]))((a, z) => ((if (a._1 + paddingX >= (paddingX * 11 + 234)) 234 else a._1 + paddingX), (if (a._1 + paddingX >= (paddingX * 11 + 234)) {
      if ((a._3 % 2) == 0) a._2 + paddingY._1 else a._2 + paddingY._2
    } else {
      a._2
    }), a._3 + 1, Array(NumberBox(z, Position(a._1, a._2, 36, 90), roulette.getBoolean("roulette.debug"))) ++ a._4))._4 ++ InsideBet.splits.takeRight(24).foldLeft((160, 450, Array.empty[NumberBox]))((a, z) => (if (a._1 + paddingX >= (paddingX * 12 + 160)) 160 else a._1 + paddingX, if (a._1 + paddingX >= (paddingX * 12 + 160)) a._2 + cornerPadY else a._2, Array(NumberBox(z, Position(a._1, a._2, 70, 45), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ InsideBet.corners.foldLeft((234, 450, Array.empty[NumberBox]))((a, z) => (if (a._1 + cornerPadX >= (cornerPadX * 11 + 234)) 234 else a._1 + cornerPadX, if (a._1 + cornerPadX >= (cornerPadX * 11 + 234)) a._2 + cornerPadY else a._2, Array(NumberBox(z, Position(a._1, a._2, 36, 45), roulette.getBoolean("roulette.debug"))) ++ a._3))._3  ++ Array(NumberBox(InsideBet.topLine, Position(120, 732, 37, 28), roulette.getBoolean("roulette.debug"))) ++ InsideBet.streets.foldLeft((160, 324 + streetPadY, Array.empty[NumberBox]))((a, z) => (a._1 + streetPadX, a._2, Array(NumberBox(z, Position(a._1, a._2, 70, 28), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ InsideBet.doubleStreet.foldLeft((234, 324 + streetPadY, Array.empty[NumberBox]))((a, z) => (a._1 + streetPadX, a._2, Array(NumberBox(z, Position(a._1, a._2, 36, 28), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ Array(ColumnOne(), ColumnTwo(), ColumnThree()).foldLeft((144, 262, Array.empty[NumberBox]))((a, z) => (a._1 + 437, a._2, Array(NumberBox(z, Position(a._1, a._2, 434, 62), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ Array(RowOne(), RowTwo(), RowThree()).foldLeft((1459, 332, Array.empty[NumberBox]))((a, z) => (a._1, a._2 + 145, Array(NumberBox(z, Position(a._1, a._2, 105, 135), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ Array(Manque(), Even(), Red(), Black(), Odd(), Passe()).foldLeft((144, 193, Array.empty[NumberBox]))((a, z) => (a._1 + 219, a._2, Array(NumberBox(z, Position(a._1, a._2, 214, 62), roulette.getBoolean("roulette.debug"))) ++ a._3))._3 ++ Array(NumberBox(ThirdWheel(CalledBet.thirdWheel), Position(447, 866, 216, 95), roulette.getBoolean("roulette.debug")), NumberBox(Orphans(CalledBet.orphans), Position(692, 866, 216, 102), roulette.getBoolean("roulette.debug")), NumberBox(NeighboursOfZero(CalledBet.neighborsOfZero), Position(912, 866, 242, 104), roulette.getBoolean("roulette.debug")), NumberBox(ZeroGame(CalledBet.zeroGame), Position(1186, 866, 101, 95), roulette.getBoolean("roulette.debug"))) ++ Array(11, 36, 13, 27, 6, 34, 17, 25, 2, 21, 4, 19, 15, 32).map(Neighbors).foldLeft((464, 777, Array.empty[NumberBox]))((z, a) => (z._1 + 57, z._2, z._3 ++ Array(NumberBox(a, Position(z._1, z._2, 51, 79), roulette.getBoolean("roulette.debug")))))._3 ++ Array(5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35).map(Neighbors).foldLeft((469, 972, Array.empty[NumberBox]))((z, a) => (z._1 + 50, z._2, z._3 ++ Array(NumberBox(a, Position(z._1, z._2, 44, 77), roulette.getBoolean("roulette.debug")))))._3 ++ Array(NumberBox(Neighbors(10), Position(396, 979, 68, 52), roulette.getBoolean("roulette.debug")), NumberBox(Neighbors(23), Position(336, 919, 69, 49), roulette.getBoolean("roulette.debug")), NumberBox(Neighbors(8), Position(336, 863, 71, 45), roulette.getBoolean("roulette.debug")), NumberBox(Neighbors(30), Position(393, 794, 65, 56), roulette.getBoolean("roulette.debug")), NumberBox(Neighbors(0), Position(1264, 804, 78, 59), roulette.getBoolean("roulette.debug")), NumberBox(Neighbors(26), Position(1323, 879, 74, 63), roulette.getBoolean("roulette.debug")), NumberBox(Neighbors(3), Position(1269, 969, 82, 57), roulette.getBoolean("roulette.debug")))),
      new HighLights(Array((StraightUp(0), BetHighLight(TextureProperties(skin.getRegion("zero"), 10, 310)))) ++ (1 to 36).foldLeft((128, 310, Array.empty[(Bet, BetHighLight)]))((a, z) => (if (z % 2 != 0) {
        if ((a._2 + highLightsPadding._3) >= highLightsPadding._4 * 5) a._1 + highLightsPadding._1 else a._1
      } else {
        if (a._2 + highLightsPadding._4 >= highLightsPadding._4 * 5) a._1 + highLightsPadding._2 else a._1
      }, if (z % 2 != 0) {
        if (a._2 + highLightsPadding._3 >= highLightsPadding._4 * 5) 310 else a._2 + highLightsPadding._3
      } else {
        if (a._2 + highLightsPadding._4 >= highLightsPadding._4 * 5) 310 else a._2 + highLightsPadding._4
      }, a._3 ++ Array((StraightUp(z), BetHighLight(TextureProperties(skin.getRegion(BetConstant.RED().contains(z) match { case true => "red" case false => "black" }), a._1, a._2))))))._3 ++ Array(11, 36, 13, 27, 6, 34, 17, 25, 2, 21, 4, 19, 15, 32).map(Neighbors).foldLeft((445, 760, 0, Array.empty[(Bet, BetHighLight)]))((z, a) => (if (z._3 % 2 == 0) z._1 + 52 else z._1 + 62, z._2, z._3 + 1, z._4 ++ Array((a, BetHighLight(TextureProperties(skin.getRegion("track_three_red"), z._1, z._2))))))._4 ++ Array(5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35).map(Neighbors).foldLeft((446, 949, 0, Array.empty[(Bet, BetHighLight)]))((z, a) => (if (z._3 % 2 == 0) z._1 + 53 else z._1 + 47, z._2, z._3 + 1, z._4 ++ Array((a, BetHighLight(TextureProperties(skin.getRegion(if (z._3 % 2 == 0) "track_one_red" else "track_one_black"), z._1, z._2))))))._4 ++ Array((Neighbors(10), BetHighLight(TextureProperties(skin.getRegion("track_four_black_up"), 350, 938))), (Neighbors(23), BetHighLight(TextureProperties(skin.getRegion("track_four_red_up"), 307, 893))), (Neighbors(8), BetHighLight(TextureProperties(skin.getRegion("track_four_black_down"), 308, 800))), (Neighbors(30), BetHighLight(TextureProperties(skin.getRegion("track_four_red_down"), 346, 755))), (Neighbors(3), BetHighLight(TextureProperties(skin.getRegion("track_two_red"), 1243, 926))), (Neighbors(26), BetHighLight(TextureProperties(skin.getRegion("track_two_black"), 1291, 825))), (Neighbors(0), BetHighLight(TextureProperties(skin.getRegion("track_two_green"), 1243, 760))))))
  }
}

//InsideBet.streets.foldLeft((160, 324 + streetPadY, Array.empty[NumberBox]))((a, z) => (a._1 + streetPadX, a._2, Array(NumberBox(z, Position(a._1, a._2, 70, 28), roulette.getBoolean("roulette.debug"))) ++ a._3))._3
//InsideBet.doubleStreet.foldLeft((234, 324 + streetPadY, Array.empty[NumberBox]))((a, z) => (a._1 + streetPadX, a._2, Array(NumberBox(z, Position(a._1, a._2, 36, 28), roulette.getBoolean("roulette.debug"))) ++ a._3))._3