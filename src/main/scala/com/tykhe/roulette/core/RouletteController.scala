package com.tykhe.roulette.core

import akka.actor.Actor
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Music.OnCompletionListener
import com.badlogic.gdx.scenes.scene2d.{Event, EventListener}
import com.badlogic.gdx.utils.Timer.Task
import com.tykhe.app.{Credited, StopTImer, ShowingWinHighlights, WheelStop}
import com.tykhe.roulette.TrackNeighbors
import com.tykhe.roulette.bets._
import com.tykhe.roulette.components.buttons.Denom
import com.tykhe.roulette.components._
import com.tykhe.roulette.components.meter.{Meter, Meters}
import com.tykhe.roulette.simulation.RouletteActor

import scala.collection.mutable.ListBuffer

/**
 * Created by aravind on 21/5/15.
 */
case class RouletteController(felts:(NumberBoxes,HighLights),buttons: Buttons,meters:Meters,gloveTween: GloveTween,rouletteActor: RouletteActor){
  var lastWinNombre=0
  var countAmount:Float=0f
  var box:Int=0
  felts._1.addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      event match {
        case CallBetPressed(b)=>b match {
          case ZeroGame(a)=>felts._2.showHighLights(a.foldLeft(Array.empty[Bet]){(z,a)=> (1 to a.value._2).foreach(z=>felts._1.numBoxes.find(_.prop._1==a.value._1).head.fire(ProcessCallBet())); z++HighLights.prepareHighlights(a.value._1)})
          case NeighboursOfZero(a)=>felts._2.showHighLights(a.foldLeft(Array.empty[Bet]){(z,a)=> (1 to a.value._2).foreach(z=>felts._1.numBoxes.find(_.prop._1==a.value._1).head.fire(ProcessCallBet())); z++HighLights.prepareHighlights(a.value._1)})
          case ThirdWheel(a)=>felts._2.showHighLights(a.foldLeft(Array.empty[Bet]){(z,a)=> (1 to a.value._2).foreach(z=>felts._1.numBoxes.find(_.prop._1==a.value._1).head.fire(ProcessCallBet())); z++HighLights.prepareHighlights(a.value._1)})
          case Orphans(a)=>felts._2.showHighLights(a.foldLeft(Array.empty[Bet]){(z,a)=> (1 to a.value._2).foreach(z=>felts._1.numBoxes.find(_.prop._1==a.value._1).head.fire(ProcessCallBet())); z++HighLights.prepareHighlights(a.value._1)})
          case MaxBet(a)=>felts._2.showHighLights(a.foldLeft(Array.empty[Bet]){(z,a)=> (1 to a.value._2).foreach(z=>felts._1.numBoxes.find(_.prop._1==a.value._1).head.fire(ProcessCallBet())); z++HighLights.prepareHighlights(a.value._1)})
        }
        case NeighborsBetPressed(c)=>felts._2.showHighLights(TrackNeighbors.takeNeighbors(c).map(StraightUp).foldLeft((Array.empty[Bet])){(z,a)=>felts._1.numBoxes.find(_.prop._1==a).head.fire(ProcessCallBet()); z++HighLights.prepareHighlights(a)++ Array(Neighbors(a.value)) })
        case ClearCalledBet(z,a,f)=> z match {
          case c:CalledBet=> f match {
            case true =>felts._2.showHighLights(CalledBet.processBet(c).foldLeft(Array.empty[Bet])((j,b)=>  j++ HighLights.prepareHighlights(felts._1.numBoxes.find(_.prop._1==b.value._1).foldLeft(b.value._1){(w,e)=>(1 to b.value._2).foreach(t=>e.fire(ProcessUndo(a,true)));w })))
            case false =>CalledBet.processBet(c).foldLeft(0)((j,b)=>felts._1.numBoxes.find(_.prop._1==b.value._1).head.amount >= a*b.value._2 match {case true=>j+1 case false=>j })==CalledBet.processBet(c).length match {
              case true=>CalledBet.processBet(c).foreach(e=>felts._1.numBoxes.find(_.prop._1==e.value._1).foreach(j=>(1 to e.value._2).foreach(k=>j.fire(ProcessUndo(a,false)))))
                felts._2.showHighLights(HighLights.prepareHighlights(c))
              case false=>println("NO CALLER FOUND MATCH ON ")
            }
          }
          case n:NeighborsBet=>f match {
            case true=>felts._2.showHighLights(TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).map(StraightUp).foldLeft(Array.empty[Bet]){(j,b)=>felts._1.numBoxes.find(_.prop._1==b).head.fire(ProcessUndo(a,true));j++Array(b)})
            case false=>TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).map(StraightUp).foldLeft(0)((j,b)=>felts._1.numBoxes.find(_.prop._1==b).head.amount >= a*1 match {case true=>j+1 case false=>j}) == TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length match {
              case true=>TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).map(StraightUp).foreach(e=>felts._1.numBoxes.find(_.prop._1==e).head.fire(ProcessUndo(a,false)))
                felts._2.showHighLights(HighLights.prepareHighlights(n))
              case false=>
            }
          }
          case (x)=> felts._1.numBoxes.find(_.prop._1==x).head.fire(ProcessUndo(a,true))
            felts._2.showHighLights(HighLights.prepareHighlights(x))
        }
        case BetPressed(a)=> felts._2.showHighLights(HighLights.prepareHighlights(a))
        case UpdateNeighboursBet()=> meters.updateNeighboursMeter()
        case _=>
      }
      meters.addBetAmount(Meter.betMeter)
      false
    }
  })
  //need attention while removing adding chips and Need Derivation on NumberBox
  buttons.addListener(new EventListener {
    override def handle(event: Event): Boolean = {
      event match {
        case ButtonPressed(a)=> a.split("-").head match {
          case "undo"=>
//            Roulette.historyOfButtonEvents.length > 0 match {
//              case false =>
//              case true =>Roulette.undoFlag=true
//                Roulette.historyOfButtonEvents.takeRight(1).head match {
//                case "BOX"=>
               Roulette.clearBets.isEmpty match {
                  case false => Roulette.clearBets.takeRight(1).foreach(e => felts._1.numBoxes.find(_.prop._1 == e._1).head.fire(ProcessClearUndo(e._1, e._2)))
                    Roulette.nombres += Roulette.clearBets.takeRight(1).head
                    Roulette.clearBets = Roulette.clearBets.dropRight(1)
                  case true => Roulette.nombres.isEmpty match {
                    case false => Roulette.nombres.takeRight(1).foreach(e => e._1 match {
                      case c: CalledBet =>
                        val currentBet=felts._1.numBoxes.filter(_.prop._1==e._1).head
                        CalledBet.processBet(c).foldLeft(0)((z, a) => z + a.value._2) match {
                        case (x) => Meter.betMeter >= x * e._2 match {
                          case true =>
                            Meter.betMeter -= x * e._2
                            meters.addBetAmount(Meter.betMeter)
                            CalledBet.processBet(c).foreach(z => felts._1.numBoxes.find(_.prop._1 == z.value._1).foreach(f => (1 to z.value._2).foreach(x => f.fire(ProcessUndo(e._2, false)))))
                            currentBet.callBetCount-=1
                            Roulette.nombres = Roulette.nombres.dropRight(1)
                            Roulette.nombres.length > 0 match {
                              case true => felts._2.showHighLights(HighLights.prepareHighlights(Roulette.nombres.takeRight(1).head._1))
                              case false =>
                            }
                          case false =>
                        }
                      }
                      case n: NeighborsBet =>
                        val currentBet=felts._1.numBoxes.filter(_.prop._1==e._1).head
                        if (currentBet.neighborsBetCount.length > 0) TrackNeighbors.siblings=currentBet.neighborsBetCount.takeRight(1).head
                        TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).length match {
                        case (x) => Meter.betMeter >= x * e._2 match {
                          case true =>
                            currentBet.fire(UpdateNeighboursBet())
                            Meter.betMeter -= x * e._2
                            meters.addBetAmount(Meter.betMeter)
                            TrackNeighbors.takeNeighbors(n.asInstanceOf[Neighbors].value).map(StraightUp).foreach(z => felts._1.numBoxes.find(_.prop._1 == z).head.fire(ProcessUndo(e._2, false)))
                            currentBet.neighborsBetCount=currentBet.neighborsBetCount.dropRight(1)
                            Roulette.nombres = Roulette.nombres.dropRight(1)
                            Roulette.nombres.length > 0 match {
                              case true => felts._2.showHighLights(HighLights.prepareHighlights(Roulette.nombres.takeRight(1).head._1))
                              case false =>
                            }
                          case false =>
                        }
                      }
                      case (x) => Meter.betMeter >= e._2 match {
                        case true =>
                          Meter.betMeter -= e._2
                          meters.addBetAmount(Meter.betMeter)
                          felts._1.numBoxes.find(_.prop._1 == x).head.fire(ProcessUndo(e._2, false))
                          Roulette.nombres = Roulette.nombres.dropRight(1)
                          Roulette.nombres.length > 0 match {
                            case true => felts._2.showHighLights(HighLights.prepareHighlights(Roulette.nombres.takeRight(1).head._1))
                            case false =>
                          }
                        case false =>
                      }
                    })
                    case true =>
                  }
//                }
//                case "DOUBLE"=>
//                case "MAXBET"=>
//                case "REBET"=> Roulette.lastBetPlace.length > 0 && Meter.betMeter >= Roulette.lastBetAmount match {
//                  case true=> Roulette.lastBetPlace.foreach(e=> felts._1.numBoxes.find(_.prop._1==e._1).head.clearBettedChip(e._1,e._2))
//                  case false=>
//                }
//              }
//                Roulette.historyOfButtonEvents=Roulette.historyOfButtonEvents.dropRight(1)
//                Roulette.undoFlag=false
            }
          case "double"=>
            Roulette.buttonFlag=true
            Roulette.historyOfButtonEvents+=("DOUBLE")
            Roulette.buttonFlag=false
          case "rebet"=>println(a)
            Roulette.buttonFlag=true
            Roulette.historyOfButtonEvents+=("REBET")
            Roulette.lastBetPlace.length > 0 && Roulette.lastBetAmount <=Meter.creditMeter match {
              case true=>Roulette.lastBetPlace.foreach(e=>felts._1.numBoxes.find(_.prop._1==e._1).head.fire(ProcessClearUndo(e._1,e._2)))
                Roulette.lastBetPlace.foldLeft(Roulette.nombres)((a,z)=>a+=z)
              case false=>
            }
            Roulette.buttonFlag=false
          case "clearall"=>felts._1.numBoxes.foreach(e=>e.amount > 0 match {
            case true=>e.smallChip.foreach(z=>e.removeActor(z))
              e.smallChip.clear()
              e.reset
            case false=>
          })
            Meter.betMeter=0
            meters.addBetAmount(Meter.betMeter)
            Roulette.reset()
            felts._2.reset
          case "max"=>Roulette.buttonFlag=true
            Roulette.historyOfButtonEvents+=("MAXBET")
            Meter.creditMeter >= CalledBet.processBet(MaxBet(CalledBet.maxBet)).foldLeft(0)((z, a) => z + a.value._2) * Denom.currentDenomination match {
              case true =>
                Meter.betMeter += CalledBet.processBet(MaxBet(CalledBet.maxBet)).foldLeft(0)((z, a) => z + a.value._2) * Denom.currentDenomination.toInt
                felts._1.fire(CallBetPressed(MaxBet(CalledBet.maxBet)))
              case false =>
            }
            Roulette.buttonFlag=false
          case "menu"=>
            Roulette.rouletteState match {
              case STOP=> Roulette.rouletteState=PLAY
                Meter.creditMeter = Meter.creditMeter+5000
            meters.addCreditAmount(Meter.creditMeter)
                rouletteActor.operateApplication(Credited())
              case _=>Meter.creditMeter=Meter.creditMeter+5000
               meters.addCreditAmount(Meter.creditMeter)
            }
          case "help"=>rouletteActor.operateApplication(StopTImer())
          case _=>println(a)
        }
        case _=>
      }
      false
    }
  })
  def noMoreBets(valueReturn:Int)={
    gloveTween.addListener(new EventListener {
      override def handle(event: Event): Boolean = {
        event.isInstanceOf[TweenCompleted] match {case true=>wheelRolled(valueReturn);gloveTween.removeListener(this) case false=>}
        false
      }
    })
    felts._1.groupSounds.apply("noBets").playSound
    gloveTween.motionTween
  }
  def startTimer()={
    gloveTween.timerMeterHandler.addCaptureListener(new EventListener {
      override def handle(event: Event): Boolean = {
        event.isInstanceOf[FireWheelRoll]match {
          case true=> rouletteActor.operateApplication(WheelStop())
            gloveTween.timerMeterHandler.removeTimeEventListener()
            gloveTween.timerMeterHandler.stopTimer()
            gloveTween.timerMeterHandler.removeListener(this)
          case false=>
        }
        false
      }
    })
    gloveTween.timerMeterHandler.startTimer()
  }
  def stopTimer()=gloveTween.timerMeterHandler.stopTimer()
 private  def wheelRolled(drawn:Int)={
    meters.historyMeter.addDrawnNumber(drawn)
    box=0
    countAmount=0F;lastWinNombre=drawn
    gloveTween.dolly.enable(felts._2.showHighLights(Array(StraightUp(drawn))).head)
    val result=WinPrediction.predictWin(drawn)
//    result.foreach(print)
    result.foreach{possible=> countAmount+=felts._1.numBoxes.filter(e=>e.amount > 0).filter(f=> f.prop._1==possible).foldLeft(0f)((z,b)=> z+addBetWin(b))}
    Roulette.lastBetAmount=Meter.betMeter
    Meter.creditMeter= Meter.creditMeter -Meter.betMeter < 0 match {case true=>0 case false=> Meter.creditMeter - Meter.betMeter}
    meters.addCreditAmount(Meter.creditMeter)
    Meter.winMeter=countAmount.toInt
    box <= 0 match {
      case true=>felts._1.numBoxes.foldLeft(0.0F){(z,e)=> e.createActions(1000,0);z+e.amount} > 0 match {
        case true=>felts._1.groupSounds.apply("sweepChips").playSound
        case false=>
      }
        rouletteActor.operateApplication(ShowingWinHighlights())
      case false=>
        felts._1.numBoxes.foreach(e=>e.flag match {case true=> case false=>e.createActions(1000,0)})
        felts._1.timer.scheduleTask(new Task {
          override def run(): Unit = {
            felts._1.numBoxes.foreach(e=>e.flag match {case true=>e.createActions(0,-1000) case false=>})
            meters.addWinAmount(Meter.winMeter)
            Meter.creditMeter+=Meter.winMeter
            meters.addCreditAmount(Meter.creditMeter)
            Meter.winMeter > 0 match {
              case true=>felts._1.groupSounds.apply("winChips").playSound
              case false=>
            }
            rouletteActor.operateApplication(ShowingWinHighlights())
          }
        },2f)

    }

  }
  def addBetWin(nombreBox: NumberBox):Float={
    box+=1
    val amount=PayOut.payOutCalculation(nombreBox.prop._1,nombreBox.amount.toInt)
    val res=Denom.denominationValue.reverse.foldLeft((amount.toInt,Array.empty[(Float,Int)]))((a,z)=>(a._1-(a._1/z.toInt)*z.toInt,a._2++ Array((z,a._1/z.toInt))))
    res._2.filter(_._2 > 0).foreach(e=>Seq.fill(e._2){e._1}.foldLeft(0f)((a,z)=>nombreBox.addWinningChips(z,a)))
    nombreBox.flag=true
    amount
  }
}
