package com.tykhe.roulette.simulation


import java.awt.{Event, Dimension, Toolkit}
import java.beans.EventHandler
import java.nio.IntBuffer
import java.util

import akka.actor.{ActorRef, Actor}
import com.badlogic.gdx.Application.ApplicationType
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.{Texture, GL20}
import com.badlogic.gdx.scenes.scene2d
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.{Gdx, ApplicationAdapter}
import com.badlogic.gdx.graphics.g2d.{TextureRegion, PolygonSpriteBatch}
import com.badlogic.gdx.scenes.scene2d.{EventListener, Stage}
import com.badlogic.gdx.utils.viewport.StretchViewport
import com.tykhe.app
import com.tykhe.app._
import com.tykhe.roulette.bets._
import com.tykhe.roulette.components._
import com.tykhe.roulette.components.buttons.{DenomProperties, Denom}
import com.tykhe.roulette.components.meter.{Meter, Meters}
import com.tykhe.roulette.core._
import com.typesafe.config.{Config, ConfigFactory}
import org.lwjgl.{BufferUtils, LWJGLException}
import org.lwjgl.input.{Cursor, Mouse}

import scala.util.Random

/**
 * Created by aravind on 25/5/15.
 */

case class RouletteActor() extends Actor{
  val rng =new Random()
  var actorRef:ActorRef=null
  var rouletteApplication=RouletteApplication(RouletteProperties.width, RouletteProperties.height, RouletteProperties.roulette,this)
  def receive={
    case Initialize()=>
      actorRef=sender()
      new LwjglApplication(rouletteApplication,RouletteProperties.cfg)
      sender() ! Initialized()
    case LoadCredits()=>
    case Credited()=> actorRef ! Credited()
    case WheelStop()=>actorRef ! WheelStop()
    case ShowingWinHighlights()=>actorRef ! ShowingWinHighlights()
    case PlaceBets()=>
      Roulette.rouletteState=PLACE_BET
      rouletteApplication.rouletteController.startTimer()
    case StopNumber(value)=> rouletteApplication.updateResult(value)
    case CreditsRested()=> actorRef ! CreditsRested()
    case ClearResult()=>rouletteApplication.resetRoulette
    case StopTImer()=> Roulette.rouletteState=STOP
       rouletteApplication.rouletteController.stopTimer()
  }
   def operateApplication(event:app.Event)={
     println(event)
    self ! event
   }
}

object RouletteProperties  {
  val width = 1920
  val height = 1080
  val cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration()
  val screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize()
  val roulette = ConfigFactory.load("roulette.conf")
  cfg.title = "NumberBox-TEST"
  cfg.width = screenDimension.width
  cfg.height = screenDimension.height
  println(roulette)
    roulette.getBoolean("roulette.fullscreen") match {
      case true=>cfg.fullscreen=true
      case false=>
    }
  //  cfg.vSyncEnabled=true
//  new LwjglApplication(RouletteApplication(width, height, roulette), cfg)
}

case class RouletteApplication(width: Float, height: Float, roulette: Config,rouletteActor: RouletteActor) extends ApplicationAdapter{
  var stage: Stage = null
  var rouletteController: RouletteController = null
  var flag, screen = false
  var emptyCursor: Cursor = null

  override def create() = {
    stage = new Stage(new StretchViewport(width, height), new PolygonSpriteBatch())
    Gdx.input.setInputProcessor(stage)
    val skin=new Skin(Gdx.files.internal(roulette.getString("roulette.skinPath")))
    Roulette.chipsSoundMap=roulette.getConfigList("roulette.chipsSound").toArray[Config](Array.empty[Config]).foldLeft(Map.empty[String,Audio])((z,e)=> z ++ Map(e.getString("name")->ButtonConfig.loadAudio(e)) )
    Denom.apply(DenomProperties(Array[Float](1, 20, 50, 100, 5, 10, 500), Array("red_chip_mini", "pink_chip_mini", "cyan_chip_mini", "yellow_chip_mini", "green_chip_mini", "blue_chip_mini", "orange_chip_mini").foldRight(Array.empty[TextureRegion])((p, r) => Array(skin.getAtlas.findRegion(p)) ++ r)))
    rouletteController = RouletteController(Roulette.prepareRoulette(roulette,skin), new Buttons(roulette.getConfig("roulette"), skin),Meters(roulette.getConfig("roulette"),skin),GloveTween( Array(new Texture((Gdx.files.internal("chips/Layer 35.png"))),new Texture((Gdx.files.internal("chips/Layer 35.png")))),Dolly(new Texture(Gdx.files.internal("chips/dolly.png"))),TimerMeterHandler(TimerMeter.prepareTimerMeter(roulette.getConfig("roulette.timerMeter"),skin)) ),rouletteActor)
    stage.addActor(CustomTexture("roulette-felt.png"))
    stage.addActor(new MultipleButton(roulette.getConfig("roulette"), skin))
    stage.addActor(rouletteController.buttons)
    stage.addActor(new VolumeControlButton(roulette.getConfig("roulette"), skin))
    stage.addActor(rouletteController.felts._2)
    stage.addActor(rouletteController.gloveTween)
    stage.addActor(rouletteController.felts._1)
    stage.addActor(rouletteController.meters)
    setHWCursorVisible(roulette.getBoolean("roulette.cursor"))
  }

  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act()
    stage.draw()
    if (Gdx.input.isKeyPressed(Keys.C)) {
      flag match {
        case true =>
        case false =>
          flag = true
          addCredtisChangeState
      }
    } else if (Gdx.input.isKeyPressed(Keys.R)) {
      flag match {
        case true =>
          flag = false

        case false =>
      }
    } else if (Gdx.input.isKeyPressed(Keys.F)) {
      screen match {
        case false =>
          println("Setting Up FullScreen ON")
          val desktopDisplayMode = Gdx.graphics.getDesktopDisplayMode()
          println(desktopDisplayMode.width + "   " + desktopDisplayMode.height)
          Gdx.graphics.setDisplayMode(desktopDisplayMode.width, desktopDisplayMode.height, true)
          screen = true
        case true =>
      }
    }
    else if (Gdx.input.isKeyPressed(Keys.G)) {
      screen match {
        case true =>
          println("Setting Up FullScreen OFF")
          Gdx.graphics.setDisplayMode(width.toInt, height.toInt, false)
          screen = false
        case false =>
      }
    }
  }

  @throws(classOf[LWJGLException])
  def setHWCursorVisible(visible: Boolean): Unit = {
    if (Gdx.app.getType() != ApplicationType.Desktop
      && Gdx.app.isInstanceOf[LwjglApplication])
      return;
    if (emptyCursor == null) {
      if (Mouse.isCreated()) {
        var min: Int = org.lwjgl.input.Cursor.getMinCursorSize();
        var tmp: IntBuffer = BufferUtils.createIntBuffer(min * min);
        emptyCursor = new Cursor(min, min, min / 2,
          min / 2, 1, tmp, null);
      } else {
        throw new LWJGLException(
          "Could not create empty cursor before Mouse object is created");
      }
    }
    if (Mouse.isInsideWindow()) Mouse.setNativeCursor(if (visible) emptyCursor else null)
  }

  override def resize(width:Int,height:Int)={
    println("RESIZED BY APPLICATION ADAPTER")
    stage.getViewport.setScreenSize(width,height)
  }

  override def dispose() = {
    stage.dispose()
  }
  def addCredtisChangeState={
    Meter.creditMeter=3000
    rouletteController.meters.addCreditAmount(Meter.creditMeter)
    Roulette.rouletteState=PLACE_BET
    rouletteActor.operateApplication(Credited())
  }
  def updateResult(roll:Int)={
    Roulette.rouletteState=STOP_BET
    rouletteController.noMoreBets(roll)
    Roulette.removeMultiDenom.foreach(removeChip=> Roulette.nombres.-=(removeChip))
    Roulette.lastBetPlace = Roulette.nombres.toSeq
    Roulette.nombres.clear()
    Roulette.removeMultiDenom.clear()
    Roulette.clearBets.clear()
    Roulette.historyOfButtonEvents.clear()
    rouletteController.felts._1.numBoxes.foreach(e=>e.refSmallChips.clearChip())
  }
  def resetRoulette={
    rouletteController.felts._1.numBoxes.foreach(chip=> chip.smallChip.clear())
    Meter.betMeter=0
    Meter.winMeter=0
    rouletteController.meters.addBetAmount(Meter.betMeter)
    rouletteController.meters.addWinAmount(Meter.winMeter)
    rouletteController.gloveTween.dolly.disable()
    Roulette.rouletteState = STOP
    Roulette.undoFlag=false
    rouletteController.felts._2.reset
    rouletteActor.operateApplication(CreditsRested())
  }
}
//++ Array(ColumnOne(), ColumnTwo(), ColumnThree()).foldLeft((124, Array.empty[(Bet, BetHighLight)]))((z, a) => (z._1 + 435, z._2 ++ Array((a, BetHighLight(TextureProperties(skin.getRegion("column"), z._1, 241))))))._2 ++ Array(RowOne(), RowTwo(), RowThree()).foldLeft((314, Array.empty[(Bet, BetHighLight)]))((z, a) => (z._1 + 140, z._2 ++ Array((a, BetHighLight(TextureProperties(skin.getRegion("row"), 1441, z._1))))))._2 ++ Array(Manque(), Even(), Red(), Black(), Odd(), Passe()).foldLeft((178, Array.empty[(Bet, BetHighLight)]))((z, a) => (z._1 + 218, z._2 ++ Array((a, BetHighLight(TextureProperties(skin.getRegion("default"), z._1, 60))))))._2)