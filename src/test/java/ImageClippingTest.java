import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import org.lwjgl.opengl.GL11;

/**
 * <code>ImageClipTest</code>
 *
 */
public class ImageClippingTest {

    public static void main(String[] args) {
        ApplicationListener listener = new ApplicationListener() {
            Stage stage;

            @Override
            public void resume() {
            }

            @Override
            public void resize(int width, int height) {
                stage.getViewport().update(width,height);
            }

            @Override
            public void render() {
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

                stage.act();
                stage.draw();
            }

            @Override
            public void pause() {
            }

            @Override
            public void dispose() {
            }

            @Override
            public void create() {
                stage = new Stage();
                ImageActor actor = new ImageActor("src/main/resources/assets/wheel.png", 210);
                actor.setBounds(30, 30, 240, 210);
                stage.addActor(actor);
            }
        };
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width=500;
        config.height=700;
        new LwjglApplication(listener, config);
    }

    private static class ImageActor extends Actor {

        private Texture texture;

        private float maxHeight;

        private float currentHeight;

        public ImageActor(String file, float maxHeight) {
            texture = new Texture(Gdx.files.internal(file));
            currentHeight = this.maxHeight = maxHeight;
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            super.draw(batch, parentAlpha);
            currentHeight = (currentHeight <= 4 ? maxHeight : currentHeight - 4);
            batch.draw(texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), currentHeight, 1, 1, 0,
                    (int) getX(), (int) getY(), (int) getWidth(), (int) currentHeight, false, false);
        }
    }
}
