/**
 * Created by aravind on 17/9/15.
 */
public class Parent {
String s;
    public String show(){
     return "PARENT";
    }

    public  void show(String s){
        this.s=s;
    }

    private void disp(){
        System.out.println("PRIVATE PROPERTY");
    }

    private void disp(String s){
        this.s=s;
        System.out.println("PRIVATE PROPERTY "+s);
    }
   final  protected  String calculate(int a, int b){
        this.s= ""+(a*b);
        return s;
    }

    void hide(){
        System.out.println("HIDDEN PROPERTY ");
    }
}

class Child extends Parent{

    @Override
    public String show() {
        return super.show();
    }

    @Override
    protected   void hide() {
        super.hide();
    }

}

