package com.tykhe.roulette

import com.tykhe.roulette.bets._
import com.tykhe.roulette.core.{NombreBoxes, NombreBox, RouletteControllerTest}
import org.scalatest.junit.JUnitSuite
import org.junit.{Before, Test, Assert}


/**
 * Created by aravind on 29/4/15.
 */
abstract class Chip{
  val denominationMap=scala.collection.mutable.Map(1.0->0,2.0->0,5.0->0,10.0->0,25.0->0,50.0->0,100.0->0)
  def apply(key:Double):Int=denominationMap(key)
  def change(key:Double,value:Int):Int={denominationMap(key)=value; value}
}
case class SmallChip() extends Chip {
  def addChip(key: Double): Unit = {
    change(key,apply(key)+1)
    val sortedDenom= List(denominationMap.toSeq.sortWith(_._1 < _._1):_*).partition(_._1<=key)
    sortedDenom._1.foldLeft((0,sortedDenom._1.head._1)){(z,a)=>if(a._1==z._1){ change(z._2,0); (change(a._1,a._2+1),a._1) }else{ (z._1 + (a._1*a._2).toInt ,a._1)}}
    sortedDenom._2.length > 0 match {
      case true=>
        sortedDenom._2.foldLeft((0,sortedDenom._2.head._1)){(z,a)=>if(a._1==z._1){ change(z._2,0); (change(a._1,a._2+1),a._1) }else{ (z._1 + (a._1*a._2).toInt ,a._1)}}
        sortedDenom._2.filter(_._1==sortedDenom._1.foldLeft(0)((z,b)=>(b._1*b._2).toInt + z)) match {
          case (x)=> x.length > 0 match {
            case true =>sortedDenom._1.foreach(e=>change(e._1,0));x.foreach(z=> addChip(z._1))
            case false=>
          }
        }
      case false=>
    }
  }
}
class RouletteTest extends JUnitSuite{
  var  betBoxes:Seq[NombreBox]=_
  var chips:Array[Seq[Double]]=_
  @Before def initalize()={
    betBoxes=InsideBet.straightUps.zip(Seq.fill(36){10}).map(NombreBox) ++ InsideBet.splits.zip(Seq.fill(InsideBet.splits.size){10}).map(NombreBox)  ++ Seq(ColumnOne(),ColumnTwo(),ColumnThree()).zip(Seq(5)).map(NombreBox) ++ Seq(RowOne(),RowTwo(),RowThree()).zip(Seq(5)).map(NombreBox)
    chips=Array(Seq.fill(10){1.0}.seq++Seq.fill(15){2.0}.seq++Seq.fill(1){100.0}.seq++Seq.fill(7){5.0}.seq++Seq.fill(5){10.0}.seq++Seq.fill(2){25.0}.seq++Seq.fill(1){50.0}.seq, Seq.fill(10){1.0}.seq++Seq.fill(15){2.0}.seq++Seq.fill(2){25.0}.seq++Seq.fill(5){5.0}.seq++Seq.fill(3){50.0}.seq++Seq.fill(5){100.0}.seq ,Seq.fill(100){1.0})
  }
  @Test def payOutCalTest={
    (0 to 36 ) foreach(e=>WinPrediction.predictWin(e))
    Assert.assertTrue(WinPrediction.predictWin(0).isInstanceOf[Seq[InsideBet]])
    Assert.assertFalse(WinPrediction.predictWin(6).contains(Red()))
    Assert.assertNotSame(PayOut.payOutCalculation(Even(),30),0)
    Assert.assertNotSame(PayOut.payOutCalculation(StraightUp(5),30),0)
    Assert.assertEquals(PayOut.payOutCalculation(Split(5,6),10),170.0F,0.0f)
  }
  @Test def calledBetTest={
    Assert.assertEquals(CalledBet.zeroGame.foldLeft(0)((z,b)=> z+b.value._2),4)
    Assert.assertEquals(CalledBet.neighborsOfZero.foldLeft(0)((z,b)=> z+b.value._2),9)
    Assert.assertEquals(CalledBet.orphans.foldLeft(0)((z,b)=> z+b.value._2),5)
    Assert.assertEquals(CalledBet.thirdWheel.foldLeft(0)((z,b)=> z+b.value._2),6)
    Assert.assertEquals(CalledBet.maxBet.foldLeft(0)((z,b)=> z+b.value._2),40)

  }
  @Test def gamePlayTest={
    val game:RouletteControllerTest=new RouletteControllerTest()
    game.nomberBoxes=new NombreBoxes(betBoxes)
    game.wheelRolled(10)
    Assert.assertEquals(game.countAmount,880.0f,0.0f)
    game.showTable
  }
  //  @Test def smallChipTest={
  //    for(i <- 1 to 1000) {
  //      val smallChip = new SmallChip
  //      val chips = Seq.fill(i) {1.0}
  //      chips.foreach(chip => smallChip.addChip(chip))
  //      Assert.assertEquals("Assert are Equal", i , smallChip.denominationMap.filter(_._2 > 0).foldLeft(0)((z, b) => z + (b._1*b._2).toInt))
  //    }
  //  }
  //old Mapping Code
//  denominationMap.find(_._1 == apply(key) * key) match {
//    case Some(x) =>
//      change(key, 0)
//      change(x._1, x._2 + 1)
//    case None => change(key, apply(key) + 1)
//  }
//  val sortedDenom: List[(Float, Int)] = denominationMap.toList sortBy (_._1)
//  sortedDenom.foldLeft((0, List.empty[(Float, Int)]))((z, a) => if (a._2 > 0) (z._1 + a._1.toInt * a._2, z._2 ++ List(a)) else z) match {
//    case (r, c) => sortedDenom.find(_._1 == r) match {
//      case Some(x) => c.foreach(e => change(e._1, 0))
//        change(x._1, apply(x._1) + 1)
//      case None =>
//    }
//  }

  @Test def smallChipTestTwo={
    chips.foreach{e=>
      val smallchip=new SmallChip
      e.foreach(chip=>smallchip.addChip(chip))
      val resultNum=smallchip.denominationMap.foldLeft(0)((c,z)=> c+(z._1*z._2).toInt)
      Assert.assertEquals("Equal Amount is :" +resultNum,smallchip.denominationMap.foldLeft(0)((c,z)=> c + (e.filter(f=> f==z._1).length*z._1).toInt),resultNum)
    }
  }
}
