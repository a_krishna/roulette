package com.tykhe.roulette

/**
 * Created by aravind on 3/6/15.
 */
object Sample extends App {
  val a = Array(0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26)
  val denomMap=Map(1.0->0,2.0->0,5.0->0,10.0->0,25.0->0,50.0->0,100.0->0)
  def showArray(value: Int)(count: Int): Array[Int] = {
   def leftT(left:Int)(leftCount:Int):Array[Int]={
     val k=a.indexOf(left)
     if(leftCount==0) Array(left) else  leftT(if(k-1 < 0 ) a.takeRight(1).head else a(k-1))(leftCount-1) ++ Array(left)
   }
    def rightT(right:Int)(rightCount:Int):Array[Int]={
      val k=a.indexOf(right)
      if(rightCount==0) Array(right) else Array(right) ++ rightT(if(k+1 >= a.length ) a.head else a(k+1))(rightCount-1)
    }
   leftT(value)(count)++rightT(value)(count).drop(1)
  }
//
//  showArray(26)(1).foreach(e => print(" " + e))
//  println()
//  showArray(26)(2).foreach(e => print(" " + e))
//  println()
//  showArray(26)(3).foreach(e => print(" " + e))
//  println()
//  showArray(26)(4).foreach(e => print(" " + e))
//  println()
//  showArray(26)(5).foreach(e => print(" " + e))
  def prepareChips(z:List[(Double,Int)])(sum:Float):List[(Float,Int)]={
  val sortedMap=z.filter(_._1 < sum)
   val lastNum= sortedMap.length > 1 match {case true=>sortedMap.takeRight(1).head._1 case false if sortedMap.length == 1 => sortedMap.head._1 }
    sum % lastNum == 0 match {case true => List((lastNum.toFloat, (sum/ lastNum).toInt)) case false=> prepareChips(z.dropRight(1))(sum ) }
  }
  prepareChips(List(denomMap.toSeq.sortWith(_._1 < _._1):_*))(7.0f).foreach(println)
}
