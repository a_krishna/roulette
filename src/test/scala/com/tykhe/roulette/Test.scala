package com.tykhe.roulette

import java.awt.{Toolkit, Dimension}

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.ui.{ImageButton, Skin}
import com.badlogic.gdx.{Gdx, ApplicationAdapter}
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.StretchViewport
import com.tykhe.roulette.bets.{WinPrediction, OutsideBet, InsideBet}
import com.tykhe.roulette.components._
import com.tykhe.roulette.components.meter.Meter
import com.tykhe.roulette.core._
import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Random

/**
 * Created by aravind on 6/4/15.
 */
object Test extends App{
  val width=800
  val height=600
  val cfg: LwjglApplicationConfiguration = new LwjglApplicationConfiguration()
  val screenDimension: Dimension = Toolkit.getDefaultToolkit().getScreenSize();
  cfg.title="ROULETTE-TEST"
  cfg.width=width

  cfg.height=height
  cfg.vSyncEnabled=true
  cfg.fullscreen=false
//  new LwjglApplication(new MyApp(width,height),cfg)
//  new LwjglApplication(new DenomTest(width,height),cfg)
  new LwjglApplication(new ButtonTest(width,height),cfg)

}
class ButtonTest(screenX:Float,screenY:Float)extends ApplicationAdapter{
  var stage: Stage = null
  val roulette=ConfigFactory.load("roulette.conf")
  var flag=false
  var meter:MeterTest=null
  var timerMeter:TimerMeterHandler=null
  override def create() = {
    val skin=new Skin(Gdx.files.internal(roulette.getString("roulette.skinPath")))
    meter=new MeterTest(roulette.getConfig("roulette"),skin)
    stage = new Stage(new StretchViewport(screenX, screenY), new PolygonSpriteBatch())
//    timerMeter=TimerMeterHandler(TimerMeter.prepareTimerMeter(roulette.getConfig("roulette.timerMeter"),skin))
//    stage.addActor(new VolumeControlButton(roulette.getConfig("roulette"),skin))
//    stage.addActor(new MultipleButton(roulette.getConfig("roulette"),skin))
    stage.addActor(meter.testMeter)
//    stage.addActor(meter.historyMeter)
//    stage.addActor(meter.neighbourMeter)
//    stage.addActor(timerMeter)
    Gdx.input.setInputProcessor(stage)
  }

  override def render() = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    stage.act(Gdx.graphics.getDeltaTime())
    stage.draw()
    if(Gdx.input.isKeyPressed(Keys.C)){
      flag match {
        case true=>
        case false=>
          flag=true
          val a=Random.nextInt(100000)
          val b=Random.nextInt(37)
          println("ADD Amount "+a)
          meter.addAmount(988685424)
//          meter.historyMeter.addDrawnNumber(b)
//          timerMeter.startTimer()
      }
    }else if(Gdx.input.isKeyPressed(Keys.R)){
      flag match {
        case true=>
          flag=false
//          timerMeter.stopTimer()
        case false=>
      }
    }

  }

  override def dispose() = {
    stage.dispose()
  }
  
}
