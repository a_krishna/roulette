package com.tykhe.roulette.core

import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.tykhe.roulette.components.meter.{NeighborsSbilingMeters, MeterLabel, HistoryMeter, Meter}
import com.typesafe.config.Config

/**
 * Created by aravind on 26/5/15.
 */
class MeterTest(config: Config,skin: Skin){
val testMeter=Meter(Meter.prepareMeter(config.getConfig("creditTest"),skin),Array[Char](0))
val historyMeter=HistoryMeter((0 to 10 ).foldLeft((config.getInt("historyMeter.fontY"),Array.empty[MeterLabel]))((a,z)=>(a._1-config.getInt("historyMeter.padY"),a._2++getMeterLabel(a._1)))._2,0)
  val neighbourMeter=new NeighborsSbilingMeters(config.getInt("neighboursButton.max"),(Meter.prepareMeter(config.getConfig("neighboursButton"),skin),Array[Char](0)))
  def addAmount(amt:Int)={
    testMeter.setAmount(amt)
  }
  def getMeterLabel(padY:Int):Array[MeterLabel]={
    val meter=MeterLabel(Meter.prepareLabels(config.getConfig("historyMeter"),skin),Array[Char](0))
    meter.changePosition(meter.getX,padY)
    Array(meter)
  }
}
