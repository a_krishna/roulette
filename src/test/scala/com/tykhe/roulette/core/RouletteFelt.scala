package com.tykhe.roulette.core

import com.tykhe.roulette.bets.{PayOut, WinPrediction, Bet}

import scala.collection.mutable.ListBuffer

/**
 * Created by aravind on 29/4/15.
 */
case class PlayerHand(boxes:Seq[NombreBox])
case class NombreBox(value:(Bet,Int)){
  def showBox=print(" BetType "+value._1+" amount "+value._2)
}
class NombreBoxes(boxes:Seq[NombreBox]){
  var playerHand:PlayerHand=PlayerHand(boxes)
  def showTotalBet=print("TOTAL BET ="+boxes.foldLeft(0)((z,b)=>z+b.value._2))
  def showHandWin=println("WIN "+ playerHand.boxes.foldLeft(0)((z,b)=>z+b.value._2))
}
class RouletteControllerTest(){
  var lastWinNombre=0
  var nomberBoxes:NombreBoxes=null
  var countAmount:Float=0f
  var box:ListBuffer[NombreBox]=new ListBuffer[NombreBox]()
  def wheelRolled(value:Int)={
    countAmount=0F;lastWinNombre=value
    WinPrediction.predictWin(value).foreach{possible=> countAmount+=nomberBoxes.playerHand.boxes.filter(e=>e.value._2 > 0).filter(f=> f.value._1==possible).foldLeft(0f)((z,b)=> z+addBetWin(b))}
  }
  def addBetWin(nombreBox: NombreBox):Float={
    box.+=(nombreBox)
    PayOut.payOutCalculation(nombreBox.value._1,nombreBox.value._2)
  }
  def showTable= {
    print(nomberBoxes.showTotalBet + " Number Drawn =" + lastWinNombre + " BetsWinCount =" + box.size + " Total Win Amount =" + countAmount + " Type of Bet [")
    println(box.foreach(print))
  }
}
